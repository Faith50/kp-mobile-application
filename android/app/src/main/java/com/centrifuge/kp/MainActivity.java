package com.centrifuge.kp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.neurotec.biometrics.NBiometricStatus;
import com.neurotec.biometrics.NFinger;
import com.neurotec.biometrics.NMatchingSpeed;
import com.neurotec.biometrics.NSubject;
import com.neurotec.biometrics.NTemplateSize;
import com.neurotec.biometrics.client.NBiometricClient;
import com.neurotec.devices.NDeviceManager;
import com.neurotec.devices.NDeviceType;
import com.neurotec.io.NFile;
import com.neurotec.lang.NCore;
import com.neurotec.licensing.NLicenseManager;
import com.neurotec.licensing.gui.ActivationActivity;
import com.neurotec.licensing.gui.LicensingPreferencesFragment;
import com.neurotec.plugins.NDataFileManager;
import com.neurotec.samples.licensing.LicensingManager;
import com.neurotec.samples.util.FileUtils;
import com.neurotec.samples.util.NImageUtils;
import com.neurotec.util.concurrent.CompletionHandler;

import java.io.File;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;

import io.flutter.embedding.android.FlutterFragmentActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterFragmentActivity {
    private static final String CHANNEL = "kp.centrifugegroup/scanner";
    private static final String[] LICENSES = new String[]{"FingerMatcher", "FingerClient"};

    private NBiometricClient mBiometricClient;
    private MethodChannel.Result pendingMethodChannelResult;

    private CompletionHandler<NBiometricStatus, NSubject> completionHandler = new CompletionHandler<NBiometricStatus, NSubject>() {
        @Override
        public void completed(NBiometricStatus result, NSubject subject) {
            if (result == NBiometricStatus.OK) {
                boolean imageSaved = false;
                boolean templateSaved = false;
                String id = UUID.randomUUID().toString();
                Log.i("CAPTURE_TASK", "Template created");
                File baseDirectory = new File(getFilesDir(), "finger_prints");
                File fileOutput = new File(baseDirectory, id + ".jpeg");
                if (!baseDirectory.exists()) {
                    baseDirectory.mkdir();
                }
                try {
                    subject.getFingers().get(0).getImage().save(fileOutput.getAbsolutePath());
                    imageSaved = true;
                } catch (Exception e) {
                    Log.e("CAPTURE_TASK", e.toString());
                }

                File baseDirectory2 = new File(getFilesDir(), "finger_templates");
                if (!baseDirectory2.exists()) {
                    baseDirectory2.mkdir();
                }
                File fileOutput2 = new File(baseDirectory2, id + ".dat");
                try {
                    NFile.writeAllBytes(fileOutput2.getAbsolutePath(), subject.getTemplateBuffer());
                    templateSaved = true;
                } catch (Exception e) {
                    Log.e("CAPTURE_TASK", e.toString());
                }

                mBiometricClient.cancel();

                if (templateSaved && imageSaved) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pendingMethodChannelResult.success(fileOutput.getAbsolutePath());
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pendingMethodChannelResult.success(null);
                        }
                    });
                }
            } else {
                Log.i("CAPTURE_TASK", "Template not created");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pendingMethodChannelResult.success(null);
                    }
                });
            }
        }

        @Override
        public void failed(Throwable exc, NSubject subject) {
            exc.printStackTrace();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);
        try {
            NCore.setContext(this);
            NDataFileManager.getInstance().addFromDirectory("data", false);
            NLicenseManager.setTrialMode(LicensingPreferencesFragment.isUseTrial(this));
            NLicenseManager.getWritableStoragePath();
            System.setProperty("jna.nounpack", "true");
            System.setProperty("java.io.tmpdir", getCacheDir().getAbsolutePath());
        } catch (Exception e) {
            Log.e("MAIN_ACTIVITY", "Exception", e);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1 && pendingMethodChannelResult != null) {
            pendingMethodChannelResult.success(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED);
        }
    }

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
                .setMethodCallHandler(
                        (call, result) -> {
                            if (call.method.equals("obtainLicenses")) {
                                new CheckLicenseTask().execute(result);
                            } else if (call.method.equals("fingerCaptureTask")) {
                                new FingerCaptureTask().execute(result);
                            } else if (call.method.equals("bioPermissionCheck")) {
                                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                                        PackageManager.PERMISSION_GRANTED) {
                                    result.success(true);
                                } else {
                                    result.success(false);
                                }
                            } else if (call.method.equals("bioPermissionRequest")) {
                                try {
                                    String[] permissions;
                                    permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                    requestPermissions(permissions, 1);
                                    pendingMethodChannelResult = result;
                                } catch (Exception e) {
                                    result.success(false);
                                }
                            } else if (call.method.equals("verifyFingerTask")) {
                                new FingerVerificationTask().execute(result, call);
                            } else if (call.method.equals("licenseManager")) {
                                try {
                                    startActivity(new Intent(this, ActivationActivity.class));
                                    result.success(true);
                                } catch (Exception e) {
                                    Log.e("LICENSE_TASK", e.toString());
                                    result.success(false);
                                }
                            } else {
                                result.notImplemented();
                            }
                        }
                );
    }

    final class CheckLicenseTask extends AsyncTask<Object, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("LICENSE_TASK", "obtaining licenses");
        }

        @Override
        protected Boolean doInBackground(Object... params) {
            MethodChannel.Result result = (MethodChannel.Result) params[0];
            try {
                List<String> obtainedLicenses = LicensingManager.getInstance()
                        .obtainLicenses(MainActivity.this, LICENSES);
                if (obtainedLicenses.size() == LICENSES.length) {
                    Log.i("LICENSE_TASK", "Licenses obtained");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            result.success(true);
                        }
                    });
                } else {
                    Log.i("LICENSE_TASK", "Failed to obntain Licenses");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            result.success(false);
                        }
                    });
                }
            } catch (Exception e) {
                Log.i("LICENSE_TASK", e.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        result.success(false);
                    }
                });
            }
            return true;
        }
    }

//
//    final class InitBioTask extends AsyncTask<Object, Void, Boolean> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            Log.i("LICENSE_TASK", "Initializing bio tasks");
//        }
//
//        @Override
//        protected Boolean doInBackground(Object... params) {
//            MethodChannel.Result result = (MethodChannel.Result) params[0];
//            try {
//                mBiometricClient = new NBiometricClient();
//                mBiometricClient.setUseDeviceManager(true);
//                mBiometricClient.getDeviceManager().setDeviceTypes(EnumSet.of(NDeviceType.FINGER_SCANNER));
//                mBiometricClient.setFingersTemplateSize(NTemplateSize.LARGE);
//                mBiometricClient.initialize();
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        result.success(true);
//                    }
//                });
//            } catch (Exception e) {
//                Log.e("LICENSE_TASK", e.toString());
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        result.success(false);
//                    }
//                });
//            }
//            return true;
//        }
//    }

    final class FingerCaptureTask extends AsyncTask<Object, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("LICENSE_TASK", "Starting Finger Capture");
        }

        @Override
        protected Boolean doInBackground(Object... params) {
            MethodChannel.Result result = (MethodChannel.Result) params[0];
            try {
                mBiometricClient = new NBiometricClient();
                mBiometricClient.setUseDeviceManager(true);
                mBiometricClient.getDeviceManager().setDeviceTypes(EnumSet.of(NDeviceType.FINGER_SCANNER));
                mBiometricClient.setFingersTemplateSize(NTemplateSize.LARGE);
                mBiometricClient.initialize();

                NSubject subject = new NSubject();
                NFinger finger = new NFinger();

                NDeviceManager deviceManager = mBiometricClient.getDeviceManager();
                NDeviceManager.DeviceCollection devices = deviceManager.getDevices();
                if (devices.size() > 0) {
                    Log.i("CAPTURE_TASK", "Found " + devices.size() + " fingerprint scanner");
                } else {
                    Log.i("CAPTURE_TASK", "No Scanners found");
                    mBiometricClient.cancel();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            result.success(null);
                        }
                    });
                    return false;
                }
                subject.getFingers().add(finger);
                Log.i("CAPTURE_TASK", "Capturing");
                pendingMethodChannelResult = result;
                mBiometricClient.createTemplate(subject, subject, completionHandler);
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        result.success(true);
//                    }
//                });
            } catch (Exception e) {
                Log.e("CAPTURE_TASK", e.toString());
                mBiometricClient.cancel();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        result.success(null);
                    }
                });
            }
            return true;
        }
    }

    final class FingerVerificationTask extends AsyncTask<Object, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("LICENSE_TASK", "Starting Finger Capture");
        }

        @Override
        protected Boolean doInBackground(Object... params) {
            MethodChannel.Result result = (MethodChannel.Result) params[0];
            pendingMethodChannelResult = result;
            MethodCall call = (MethodCall) params[1];
            try {
                Boolean newBiometricClient = call.argument("newBiometricClient");

                if (newBiometricClient) {
                    mBiometricClient = new NBiometricClient();
                    // Set matching threshold
                    mBiometricClient.setMatchingThreshold(48);
                    // Set matching speed
                    mBiometricClient.setFingersMatchingSpeed(NMatchingSpeed.HIGH);
                    // Initialize NBiometricClient
                    mBiometricClient.initialize();
                }

                NSubject reference1 = new NSubject();
                NFinger finger1 = new NFinger();
                File baseDirectory = new File(getFilesDir(), "finger_prints");
                Log.e("VERIFY_TASK", call.argument("left_thumb_id"));
                File fileOutput = new File(baseDirectory, call.argument("left_thumb_id") + ".jpeg");
                finger1.setImage(NImageUtils.fromJPEG(FileUtils.readFileToByteArray(fileOutput.getAbsolutePath())));
                reference1.getFingers().add(finger1);

                NSubject reference2 = new NSubject();
                NFinger finger2 = new NFinger();
                File baseDirectory2 = new File(getFilesDir(), "finger_prints");
                File fileOutput2 = new File(baseDirectory2, call.argument("right_thumb_id") + ".jpeg");
                finger2.setImage(NImageUtils.fromJPEG(FileUtils.readFileToByteArray(fileOutput2.getAbsolutePath())));
                reference2.getFingers().add(finger2);

                NSubject candidate = new NSubject();
                NFinger finger3 = new NFinger();
                File baseDirectory3 = new File(getFilesDir(), "finger_prints");
                File fileOutput3 = new File(baseDirectory3, call.argument("candidate_id") + ".jpeg");
                finger3.setImage(NImageUtils.fromJPEG(FileUtils.readFileToByteArray(fileOutput3.getAbsolutePath())));
                candidate.getFingers().add(finger3);

                Boolean killBiometricClient = call.argument("killBiometricClient");

                mBiometricClient.verify(candidate, reference1, null, new CompletionHandler<NBiometricStatus, Void>() {
                    @NonNull
                    @Override
                    public void completed(NBiometricStatus result, Void attachment) {
                        if (result == NBiometricStatus.OK || result == NBiometricStatus.MATCH_NOT_FOUND) {
                            if (result == NBiometricStatus.OK) {
                                if (killBiometricClient) {
                                    mBiometricClient.cancel();
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pendingMethodChannelResult.success(true);
                                    }
                                });
                            } else {
                                mBiometricClient.verify(candidate, reference2, null, new CompletionHandler<NBiometricStatus, Void>() {
                                    @NonNull
                                    @Override
                                    public void completed(NBiometricStatus result, Void attachment) {
                                        if (result == NBiometricStatus.OK || result == NBiometricStatus.MATCH_NOT_FOUND) {
                                            if (result == NBiometricStatus.OK) {
                                                if (killBiometricClient) {
                                                    mBiometricClient.cancel();
                                                }
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        pendingMethodChannelResult.success(true);
                                                    }
                                                });
                                            } else {
                                                if (killBiometricClient) {
                                                    mBiometricClient.cancel();
                                                }
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        pendingMethodChannelResult.success(false);
                                                    }
                                                });
                                            }
                                        }
                                    }

                                    @Override
                                    public void failed(Throwable exc, Void attachment) {
                                        if (killBiometricClient) {
                                            mBiometricClient.cancel();
                                        }
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pendingMethodChannelResult.success(false);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void failed(Throwable exc, Void attachment) {
                        if (killBiometricClient) {
                            mBiometricClient.cancel();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pendingMethodChannelResult.success(false);
                            }
                        });
                    }
                });
            } catch (Exception e) {
                mBiometricClient.cancel();
                Log.e("VERIFY_TASK", e.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        result.success(null);
                    }
                });
            }
            return true;
        }
    }
}


