import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kp/models/client_intake_model.dart';
import 'package:kp/util.dart';
import 'package:kp/widgets/finger_capture_dialog.dart';

class VeriFingerSDK {
  static const _platform = const MethodChannel('kp.centrifugegroup/scanner');

  static VeriFingerSDK _instance;
  static bool _initialized = false;
  VeriFingerSDK._();

  static VeriFingerSDK getInstance() {
    if (_initialized == false) {
      throw ('VeriFingerSDK not initialized');
    }
    if (_instance == null) {
      _instance = VeriFingerSDK._();
    }
    return _instance;
  }

  static Future<bool> obtainLicenses() async {
    bool allLicensesFetched = false;
    try {
      allLicensesFetched = await _platform.invokeMethod('obtainLicenses');
    } on PlatformException catch (e) {
      print(e);
    }
    return allLicensesFetched;
  }

  static Future<void> openLicenseManager(BuildContext context) async {
    Directory neuroTechDirectory =
        Directory('/storage/emulated/0/Neurotechnology/Licenses');
    bool allPermissionsAccepted = await _checkPermissions();
    if (allPermissionsAccepted == false) {
      allPermissionsAccepted = await _requestPermission();
    }
    if (allPermissionsAccepted) {
      bool exists = await neuroTechDirectory.exists();
      if (exists == false) {
        await neuroTechDirectory.create(recursive: true);
      }
      bool add = await showBasicConfirmationDialog("Add license Files", context,
          positiveLabel: "Add Files", negativeLabel: "Continue without adding");
      if (add == true) {
        FilePickerResult result = await FilePicker.platform.pickFiles(
          allowMultiple: true,
          type: FileType.custom,
          allowedExtensions: ['sn'],
        );
        if (result != null) {
          bool allValid = true;
          result.files.forEach((e) async {
            if (e.extension == 'sn') {
              File f = File(e.path);
              await f.copy("${neuroTechDirectory.path}/${e.name}");
            } else {
              allValid = false;
            }
          });
          if (allValid == false) {
            return showBasicMessageDialog(
                "Invalid license file selected", context);
          } else {
            try {
              await _platform.invokeMethod('licenseManager');
            } on PlatformException catch (e) {
              print(e);
            }
          }
        } else {
          return;
        }
      } else if (add == false) {
        try {
          await _platform.invokeMethod('licenseManager');
        } on PlatformException catch (e) {
          print(e);
        }
      }
    } else {
      throw ("This operation requires storage permission");
    }
  }

  static Future<bool> init() async {
    // if (_initialized == true) {
    //   return true;
    // }
    bool licenseObtained = await obtainLicenses();
    if (licenseObtained == false) {
      return false;
    }
    bool allPermissionsAccepted = await _checkPermissions();
    if (allPermissionsAccepted == false) {
      allPermissionsAccepted = await _requestPermission();
    }
    // if (allPermissionsAccepted) {
    //   try {
    //     _initialized = await _platform.invokeMethod('initBioTask');
    //   } on PlatformException catch (e) {
    //     print(e);
    //   }
    // }
    if (licenseObtained == true && allPermissionsAccepted == true) {
      _initialized = true;
    }
    return _initialized;
  }

  Future<String> captureFinger() async {
    String path;
    try {
      path = await _platform.invokeMethod('fingerCaptureTask');
    } on PlatformException catch (e) {
      print(e);
    }
    return path;
  }

  static Future<bool> verifyClient(
      {ClientIntake client, BuildContext context}) async {
    Future<String> captureFinger() async {
      return await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              elevation: 10,
              contentPadding: EdgeInsets.zero,
              insetPadding: EdgeInsets.zero,
              backgroundColor: Colors.transparent,
              content: FingerCaptureDialog(),
            );
          });
    }

    bool matched;
    return captureFinger().then((value) async {
      if (value == null) {
        return null;
      }
      String candidateId = value.split('/').last.split('.').first;
      try {
        matched = await _platform.invokeMethod('verifyFingerTask', {
          'left_thumb_id': client.leftThumbId,
          'right_thumb_id': client.rightThumbId,
          'candidate_id': candidateId,
          'killBiometricClient': true,
          'newBiometricClient': true
        });
      } on PlatformException catch (e) {
        print(e);
      }
      return matched;
    });
  }

  static Future<ClientIntake> findClient(
      {List<ClientIntake> clientList, BuildContext context}) async {
    Future<String> captureFinger() async {
      return await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              elevation: 10,
              contentPadding: EdgeInsets.zero,
              insetPadding: EdgeInsets.zero,
              backgroundColor: Colors.transparent,
              content: FingerCaptureDialog(),
            );
          });
    }

    bool matched;
    ClientIntake matchedClient;
    return captureFinger().then((value) async {
      if (value == null) {
        return null;
      }
      String candidateId = value.split('/').last.split('.').first;
      showDialog(
          context: context,
          builder: (context) {
            return WillPopScope(
              onWillPop: () async => false,
              child: AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 25,
                      width: 25,
                      child: CircularProgressIndicator(),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Finding Client')
                  ],
                ),
              ),
            );
          },
          barrierDismissible: false);
      for (int x = 0; x < clientList.length; x++) {
        try {
          matched = await _platform.invokeMethod('verifyFingerTask', {
            'left_thumb_id': clientList[x].leftThumbId,
            'right_thumb_id': clientList[x].rightThumbId,
            'candidate_id': candidateId,
            'killBiometricClient': x == clientList.length - 1 ? true : false,
            'newBiometricClient': x == 0 ? true : false
          });
        } on PlatformException catch (e) {
          print(e);
        }
        if (matched == true) {
          matchedClient = clientList[x];
          break;
        }
      }
      Navigator.pop(context);
      return matchedClient;
    });
  }

  static Future<bool> _checkPermissions() async {
    bool allPermissionsGranted = false;
    try {
      allPermissionsGranted =
          await _platform.invokeMethod('bioPermissionCheck');
    } on PlatformException catch (e) {
      print(e);
    }
    return allPermissionsGranted;
  }

  static Future<bool> _requestPermission() async {
    bool permissionRequested = false;
    try {
      permissionRequested =
          await _platform.invokeMethod('bioPermissionRequest');
    } on PlatformException catch (e) {
      print(e);
    }
    return permissionRequested;
  }
}
