import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:kp/api/clients_api.dart';
import 'package:kp/db/clients.dart';
import 'package:sembast/sembast.dart';

class RegistrationBackgroundUploadTask {
  static void start(BuildContext context) {
    Timer.periodic(Duration(seconds: 10), (timer) {
      try {
        ClientsDB.getInstance()
            .getClients(
                Finder(filter: Filter.equals('is_registered_online', true)))
            .then((clients) async {
          if (clients.length > 0) {
            clients.forEach((client) {
              if (client.isRegisteredOnline == true) {
                ClientsDB.getInstance().deleteClient(client.clientCode);
              }
            });
          }
        });
      } catch (err) {
        print(err);
      }
    });
  }
}
