import 'dart:convert';

class Program {
  int programId;
  String name;
  String code;
  String description;
  List<ProgramStage> programStages;

  Program(
      {this.code,
      this.description,
      this.name,
      this.programId,
      this.programStages});

  factory Program.fromJson(Map<String, dynamic> json) {
    List<ProgramStage> stages = [];
    json['programstages'].forEach((stage) {
      stages.add(ProgramStage.fromJson(stage));
    });
    return Program(
        code: json['code'],
        programId: json['program_id'],
        name: json['name'],
        description: json['description'],
        programStages: stages);
  }

  @override
  String toString() {
    return code;
  }
}

class ProgramStage {
  int programStateId;
  int programId;
  String name;
  String code;
  String description;
  Map<String, dynamic> formJson;

  ProgramStage(
      {this.code,
      this.description,
      this.formJson,
      this.name,
      this.programId,
      this.programStateId});

  factory ProgramStage.fromJson(Map<String, dynamic> json) {
    return ProgramStage(
        programId: json['program_id'],
        programStateId: json['program_stage_id'],
        name: json['name'],
        code: json['code'],
        description: json['description'],
        formJson: json['programstageform'] == null
            ? {}
            : JsonDecoder().convert(json['programstageform']['json']));
  }

  @override
  String toString() {
    return code;
  }
}
