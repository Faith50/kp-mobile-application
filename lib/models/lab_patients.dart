class LabPatient {
  String hospitalNumber;
  DateTime recentOrder;
  String prescribedBy;
  int orderNumber;
  int testId;
  String status;

  LabPatient(
      {this.hospitalNumber = "",
      this.prescribedBy = "",
      this.recentOrder,
      this.orderNumber,
      this.testId,
      this.status = ""}) {
    if (hospitalNumber == null) {
      hospitalNumber = "";
    }
    if (prescribedBy == null) {
      prescribedBy = "";
    }

    if (status == null) {
      status = "";
    }
  }
}
