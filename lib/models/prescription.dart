import 'package:kp/models/metadata.dart';
import 'package:kp/util.dart';

class Prescription {
  KpMetaData drug;
  int dose;
  KpMetaData drugUnits;
  KpMetaData drugFrequency;
  String note;
  DateTime prescriptionDate;

  Prescription(
      {this.drugUnits,
      this.drugFrequency,
      this.dose,
      this.drug,
      this.note,
      this.prescriptionDate});

  factory Prescription.formJson(Map json) {
    return Prescription(
        drug: KpMetaData.formJson(json['drugs']),
        drugFrequency: KpMetaData.formJson(json['drugfrequency']),
        drugUnits: KpMetaData.formJson(json['drugunits']),
        dose: json['dose'],
        note: json['prescription_note'],
        prescriptionDate: convertStringToDateTime(json['prescription_date']));
  }

  Map<String, dynamic> toJson() {
    return {
      'drug_id': drug.id,
      'dose': dose,
      'drug_frequency_id': drugFrequency.id,
      'drug_unit_id': drugUnits.id,
      'prescription_note': note,
      'prescription_date': prescriptionDate.toIso8601String()
    };
  }
}
