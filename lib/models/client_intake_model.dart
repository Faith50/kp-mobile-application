import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:kp/models/allergy.dart';
import 'package:kp/models/country.dart';
import 'package:kp/models/local_government.dart';
import 'package:kp/models/metadata.dart';
import 'package:kp/models/next_of_kin.dart';
import 'package:kp/models/state.dart';
import 'package:kp/models/vitals.dart';
import 'package:kp/util.dart';
import 'package:kp/models/heirachy_unit.dart';

class ClientIntake {
  KpMetaData gender;
  String surname;
  String firstName;
  String otherNames;
  String phone;
  String altPhone;
  String residentialAddress;
  KState state;
  LocalGovernment lga;
  Country country;
  DateTime dob;
  DateTime regDate;
  KpMetaData maritalStatus;
  KpMetaData occupation;
  int numberOfChildren;
  int numberOfWives;
  String clientCode;
  int registeredBy;
  String regId;
  String leftThumbId;
  String rightThumbId;
  List<Vitals> vitalsHistory;
  List<Allergy> allergies;
  bool isRegisteredOnline;
  KpMetaData disability;
  KpMetaData targetGroup;
  KpMetaData careEntryPoint;
  KpMetaData priorArt;
  KpMetaData referredFrom;
  KpMetaData religion;
  KpMetaData language;
  KpMetaData qualification;
  KpMetaData nationality;
  List<NextOfKin> nextOfKins;
  int userId;
  String uuid;
  String hospitalNum;
  String email;
  int patientId;

  ClientIntake(
      {this.maritalStatus,
      this.gender,
      this.country,
      this.altPhone,
      this.phone,
      this.occupation,
      this.dob,
      this.firstName,
      this.otherNames,
      this.surname,
      this.numberOfWives,
      this.numberOfChildren,
      this.lga,
      this.state,
      this.residentialAddress,
      this.regDate,
      this.clientCode,
      this.registeredBy,
      this.regId,
      this.vitalsHistory,
      this.leftThumbId,
      this.rightThumbId,
      this.allergies,
      this.isRegisteredOnline,
      this.disability,
      this.targetGroup,
      this.careEntryPoint,
      this.priorArt,
      this.referredFrom,
      this.uuid,
      this.userId,
      this.religion,
      this.language,
      this.qualification,
      this.nationality,
      this.email,
      this.patientId,
      this.hospitalNum});

  factory ClientIntake.fromDBJson(Map<String, dynamic> json) {
    List<Vitals> vitalsHistoryTemp = [];
    if (json['vitals_history'] != null) {
      json['vitals_history'].forEach((e) {
        vitalsHistoryTemp.add(Vitals.fromJson(e));
      });
    }
    vitalsHistoryTemp.sort((a, b) {
      return b.dateOfVital.compareTo(a.dateOfVital);
    });

    List<Allergy> allergiesTemp = [];
    if (json['allergies'] != null) {
      json['allergies'].forEach((e) {
        allergiesTemp.add(Allergy.fromJson(e));
      });
    }
    return ClientIntake(
        surname: json['surname'],
        firstName: json['firstname'],
        otherNames: json['othername'],
        dob: DateTime.fromMillisecondsSinceEpoch(json['date_of_birth']),
        gender: KpMetaData.formJson(json['gender']),
        occupation: KpMetaData.formJson(json['occupation']),
        maritalStatus: KpMetaData.formJson(json['marital_status']),
        phone: json['phone_number'],
        altPhone: json['alt_phone_number'],
        regDate:
            DateTime.fromMillisecondsSinceEpoch(json['date_of_registration']),
        clientCode: json['client_code'],
        registeredBy: json['registered_by'],
        numberOfChildren: json['no_children'],
        numberOfWives: json['no_wives'],
        residentialAddress: json['address'],
        // state: json['state'],
        // lga: json['lga'],
        regId: json['regId'],
        vitalsHistory: vitalsHistoryTemp,
        allergies: allergiesTemp,
        leftThumbId: json['left_thumb_id'],
        isRegisteredOnline: json['is_registered_online'] == null
            ? false
            : json['is_registered_online'],
        rightThumbId: json['right_thumb_id'],
        targetGroup: KpMetaData.formJson(json['target_group']),
        careEntryPoint: KpMetaData.formJson(json['care_entry_point']),
        priorArt: KpMetaData.formJson(json['prior_art']),
        referredFrom: KpMetaData.formJson(json['referred_from']),
        religion: KpMetaData.formJson(json['religion']),
        language: KpMetaData.formJson(json['language']),
        qualification: KpMetaData.formJson(json['qualification']),
        nationality: KpMetaData.formJson(json['nationality']),
        userId: json['user_id'],
        uuid: json['uuid'],
        hospitalNum: json['hospital_num'],
        email: json['email'],
        disability: KpMetaData.formJson(json['disability']));
  }

  factory ClientIntake.formServerJson(Map<String, dynamic> json) {
    return ClientIntake(
        surname: json['surname'] == null ? "" : json['surname'],
        firstName: json['firstname'] == null ? "" : json['firstname'],
        otherNames: json['otherNames'] == null ? "" : json['otherNames'],
        dob: json['dateBirth'] == null
            ? null
            : convertStringToDateTime(json['dateBirth']),
        gender: json['genders'] == null
            ? null
            : KpMetaData(
                id: json['genders']['gender_id'],
                name: json['genders']['name'],
                code: json['genders']['code'],
                description: json['genders']['description']),
        occupation: json['occupations'] == null
            ? null
            : KpMetaData(
                id: json['occupations']['occupation_id'],
                name: json['occupations']['name'],
                code: json['occupations']['code'],
                description: json['occupations']['description']),
        maritalStatus: json['maritalstatus'] == null
            ? null
            : KpMetaData(
                id: json['maritalstatus']['marital_status_id'],
                name: json['maritalstatus']['name'],
                code: json['maritalstatus']['code'],
                description: json['maritalstatus']['description']),
        phone: json['phone_number'] == null ? "" : json['phone_number'],
        altPhone:
            json['alt_phone_number'] == null ? "" : json['alt_phone_number'],
        regDate: json['dateRegistration'] == null
            ? null
            : convertStringToDateTime(json['dateRegistration']),
        clientCode:
            json['client_code'] == null ? "" : json['client_code'].toString(),
        registeredBy: json['registered_by'],
        // numberOfChildren: json['no_children'],
        // numberOfWives: json['no_wives'],
        residentialAddress: json['address'] == null ? "" : json['address'],
        // state: json['state'],
        // lga: json['lga'],
        // regId: json['regId'],
        // leftThumbId: json['left_thumb_id'],
        isRegisteredOnline: true,
        // rightThumbId: json['right_thumb_id'],
        targetGroup: json['targetgroups'] == null
            ? null
            : KpMetaData(
                id: json['targetgroups']['target_group_id'],
                name: json['targetgroups']['name'],
                code: json['targetgroups']['code'],
                description: json['targetgroups']['description']),
        careEntryPoint: json['careEntryPoint'] == null
            ? null
            : KpMetaData(
                id: json['careEntryPoint']['care_entry_point_id'],
                name: json['careEntryPoint']['name'],
                code: json['careEntryPoint']['code'],
                description: json['careEntryPoint']['description']),
        priorArt: json['priorart'] == null
            ? null
            : KpMetaData(
                id: json['priorart']['prior_art_id'],
                name: json['priorart']['name'],
                code: json['priorart']['code'],
                description: json['priorart']['description']),
        referredFrom: json['referredfrom'] == null
            ? null
            : KpMetaData(
                id: json['referredfrom']['referred_from_id'],
                name: json['referredfrom']['name'],
                code: json['referredfrom']['code'],
                description: json['referredfrom']['description']),
        religion: json['religions'] == null
            ? null
            : KpMetaData(
                id: json['religions']['religion_id'],
                name: json['religions']['name'],
                code: json['religions']['code'],
                description: json['religions']['description']),
        language: json['languages'] == null
            ? null
            : KpMetaData(
                id: json['languages']['language_id'],
                name: json['languages']['name'],
                code: json['languages']['code'],
                description: json['languages']['description']),
        qualification: json['qualifications'] == null
            ? null
            : KpMetaData(
                id: json['qualifications']['qualification_id'],
                name: json['qualifications']['name'],
                code: json['qualifications']['code'],
                description: json['qualifications']['description']),
        nationality: json['nationalities'] == null
            ? null
            : KpMetaData(
                id: json['nationalities']['nationality_id'],
                name: json['nationalities']['name'],
                code: json['nationalities']['code'],
                description: json['nationalities']['description']),
        patientId: json['patientId'],
        userId: json['userId'],
        uuid: json['idUuid'],
        hospitalNum: json['hospitalNum'] == null ? "" : json['hospitalNum'],
        email: json['email'],
        vitalsHistory: [],
        allergies: [],
        disability: json['disabilities'] == null
            ? null
            : KpMetaData(
                id: json['disabilities']['disability_id'],
                name: json['disabilities']['name'],
                code: json['disabilities']['code'],
                description: json['disabilities']['description']));
  }

  Map toJson(BuildContext context) {
    return <String, dynamic>{
      'surname': surname,
      'firstname': firstName,
      'otherNames': otherNames,
      'dateBirth': dob.toIso8601String(),
      'gender_id': gender == null ? null : gender.id,
      'occupation_id': occupation == null ? null : occupation.id,
      'marital_status_id': maritalStatus == null ? null : maritalStatus.id,
      'phone_number': phone,
      'address': residentialAddress,
      'alt_phone_number': altPhone,
      'client_code': clientCode,
      // 'registered_by': registeredBy,
      'userId': registeredBy,
      'dateRegistration': regDate.toIso8601String(),
      'disability_id': disability == null ? null : disability.id,
      'target_group_id': targetGroup == null ? null : targetGroup.id,
      'care_entry_point_id': careEntryPoint == null ? null : careEntryPoint.id,
      'prior_art_id': priorArt == null ? null : priorArt.id,
      'hospitalNum': hospitalNum,
      'language_id': language == null ? null : language.id,
      'religion_id': religion == null ? null : religion.id,
      'qualification_id': qualification == null ? null : qualification.id,
      'nationality_id': nationality == null ? null : nationality.id,
      'email': email,
    };
  }

  Map toJsonUpdate(BuildContext context) {
    Map<String, dynamic> json = toJson(context);
    json.keys.where((k) => json[k] == null).toList().forEach(json.remove);
    print(JsonEncoder().convert(json));
    json['userId'] = userId;
    json['idUuid'] = uuid;
    return json;
  }

  Map toDBJson(BuildContext context) {
    return <String, dynamic>{
      'surname': surname,
      'firstname': firstName,
      'othername': otherNames,
      'date_of_birth': dob.millisecondsSinceEpoch,
      'gender': gender.toJson(),
      'occupation': occupation.toJson(),
      'marital_status': maritalStatus.toJson(),
      'phone_number': phone,
      'alt_phone_number': altPhone,
      'date_of_registration': regDate.millisecondsSinceEpoch,
      'client_code': clientCode,
      'registered_by': registeredBy,
      'regId': regId,
      'no_children': numberOfChildren,
      'no_wives': numberOfWives,
      'address': residentialAddress,
      // 'state': state,
      // 'lga': lga,
      'left_thumb_id': leftThumbId,
      'right_thumb_id': rightThumbId,
      'is_registered_online':
          isRegisteredOnline == null ? false : isRegisteredOnline,
      'target_group': targetGroup.toJson(),
      'disability': disability.toJson(),
      'care_entry_point': careEntryPoint.toJson(),
      'prior_art': priorArt.toJson(),
      'referred_from': referredFrom.toJson(),
      'uuid': uuid,
      'user_id': userId,
      'hospital_num': hospitalNum,
      'religion': religion.toJson(),
      'language': language.toJson(),
      'qualification': qualification.toJson(),
      'nationality': nationality.toJson(),
      'email': email
    };
  }
}
