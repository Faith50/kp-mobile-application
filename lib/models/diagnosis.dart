import 'package:kp/models/metadata.dart';
import 'package:kp/util.dart';

class Diagnosis {
  String note;
  DateTime onsetDate;
  KpMetaData condition;
  KpMetaData severity;
  DateTime diagnosisDate;

  Diagnosis(
      {this.condition,
      this.diagnosisDate,
      this.note,
      this.onsetDate,
      this.severity});

  factory Diagnosis.fromJson(Map<String, dynamic> json) {
    return Diagnosis(
        note: json['clinical_note'],
        onsetDate: convertStringToDateTime(json['onset_date']),
        condition: KpMetaData(
            id: json['diagnosisconditions']['diagnosis_condition_id'],
            code: json['diagnosisconditions']['code'],
            name: json['diagnosisconditions']['name'],
            description: json['diagnosisconditions']['description']),
        severity: KpMetaData(
            id: json['severity']['severity_id'],
            code: json['severity']['code'],
            name: json['severity']['name'],
            description: json['severity']['description']),
        diagnosisDate: convertStringToDateTime(json['diagnosed_date']));
  }
}
