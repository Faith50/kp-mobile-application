class User {
  int userId;
  int userTypeId;
  String username;
  String password;

  User({this.username, this.userId, this.userTypeId, this.password});

  factory User.fromJson(Map json) {
    return User(
        userId: json['userid'],
        userTypeId: json['user_type_id'],
        username: json['username'],
        password: json['password']);
  }
}
