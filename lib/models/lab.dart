class LabTestType {
  int labTestTypeId;
  String name;
  String code;
  String description;

  LabTestType({this.name, this.code, this.labTestTypeId, this.description});

  factory LabTestType.formJson(Map<String, dynamic> json) {
    return LabTestType(
        name: json['name'],
        code: json['code'],
        labTestTypeId: json['labTestTypeId']);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'name': name,
      'code': code,
      'labTestTypeId': labTestTypeId
    };
  }

  @override
  String toString() {
    return "$labTestTypeId$name$code";
  }
}

class LabTest {
  int labTestTypeId;
  int labTestId;
  String name;
  String code;
  String description;

  LabTest(
      {this.name,
      this.code,
      this.labTestTypeId,
      this.description,
      this.labTestId});

  factory LabTest.formJson(Map<String, dynamic> json) {
    return LabTest(
        name: json['name'],
        code: json['code'],
        labTestTypeId: json['labTestTypeId'],
        labTestId: json['labTestId']);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'name': name,
      'code': code,
      'labTestTypeId': labTestTypeId,
      'labTestId': labTestId
    };
  }

  @override
  String toString() {
    return "$labTestId$labTestTypeId$name$code";
  }
}
