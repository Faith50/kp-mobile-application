class KpMetaData {
  int id;
  String name;
  String code;
  String description;

  KpMetaData({this.name, this.code, this.id, this.description});

  factory KpMetaData.formJson(Map<String, dynamic> json) {
    return KpMetaData(name: json['name'], code: json['code'], id: json['id']);
  }

  Map toJson() {
    return <String, dynamic>{'name': name, 'code': code, 'id': id};
  }

  @override
  String toString() {
    return "$id$name$code";
  }
}
