import 'package:kp/models/metadata.dart';

class Allergy {
  KpMetaData allergy;
  KpMetaData allergens;
  KpMetaData reactions;
  KpMetaData conditions;
  String treatment;
  String observation;

  Allergy(
      {this.treatment,
      this.allergens,
      this.allergy,
      this.conditions,
      this.observation,
      this.reactions});

  Map<String, dynamic> toJson() {
    return {
      'allergy': allergy.toJson(),
      'allergens': allergens.toJson(),
      'reactions': reactions.toJson(),
      'conditions': conditions.toJson(),
      'treatment': treatment,
      'observation': observation
    };
  }

  factory Allergy.fromJson(Map<String, dynamic> json) {
    return Allergy(
        allergens: KpMetaData.formJson(json['allergens']),
        allergy: KpMetaData.formJson(json['allergy']),
        reactions: KpMetaData.formJson(json['reactions']),
        treatment: json['treatment'],
        conditions: KpMetaData.formJson(json['conditions']),
        observation: json['observation']);
  }
}
