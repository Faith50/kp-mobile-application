import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:kp/api/request_middleware.dart';
import 'package:kp/db/clients.dart';
import 'package:kp/globals.dart';
import 'package:kp/models/client_intake_model.dart';

class ConsultationApi {
  static Future<bool> postClinicalDiagnosis(
      ClientIntake client, Map<String, dynamic> data) async {
    print(data);
    try {
      http.Response response = await RequestMiddleWare.makeRequest(
          method: RequestMethod.POST,
          url: endPointBaseUrl + "/postclinicaldiagnosis",
          body: JsonEncoder().convert([data]),
          headers: {'Content-Type': 'application/json'});
      if (response.statusCode == 200) {
        var data = JsonDecoder().convert(response.body);
        print(data);
        return true;
      } else {
        print(response.body);
        throw ('Error saving data');
      }
    } on SocketException catch (_) {
      throw ('Connection Error');
    }
  }

  static Future<bool> postClinicalVitals(
      ClientIntake client, Map<String, dynamic> data) async {
    print(data);
    try {
      http.Response response = await RequestMiddleWare.makeRequest(
          url: endPointBaseUrl + "/postclinicalvitalsigns",
          body: JsonEncoder().convert(data),
          headers: {'Content-Type': 'application/json'},
          method: RequestMethod.POST);
      if (response.statusCode == 200) {
        var data = JsonDecoder().convert(response.body);
        print(data);
        return true;
      } else {
        print(response.body);
        throw ('Error saving data');
      }
    } on SocketException catch (_) {
      throw ('Connection Error');
    }
  }

  static Future<bool> postClinicalAllergies(
      ClientIntake client, Map<String, dynamic> data) async {
    print(data);
    try {
      http.Response response = await RequestMiddleWare.makeRequest(
          url: endPointBaseUrl + "/postclinicalallergies",
          body: JsonEncoder().convert([data]),
          headers: {'Content-Type': 'application/json'},
          method: RequestMethod.POST);
      if (response.statusCode == 200) {
        var data = JsonDecoder().convert(response.body);
        print(data);
        return true;
      } else {
        print(response.body);
        throw ('Error saving data');
      }
    } on SocketException catch (_) {
      throw ('Connection Error');
    }
  }
}
