import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:kp/api/request_middleware.dart';
import 'package:kp/globals.dart';
import 'package:kp/models/programs.dart';

class ProgramsApi {
  static Future<List<Program>> getPrograms() async {
    try {
      String url = endPointBaseUrl + '/listprograms';
      http.Response response = await RequestMiddleWare.makeRequest(
          url: url, method: RequestMethod.GET);
      if (response.statusCode == 200) {
        List data = JsonDecoder().convert(response.body);
        List<Program> programs = data.map((prog) {
          return Program.fromJson(prog);
        }).toList();
        return programs;
      } else {
        throw ('Could not fetch forms');
      }
    } on SocketException catch (_) {
      throw ('Connection Error');
    }
  }
}
