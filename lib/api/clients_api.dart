import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:kp/api/request_middleware.dart';
import 'package:kp/db/clients.dart';
import 'package:kp/globals.dart';
import 'package:kp/models/client_intake_model.dart';

class ClientApi {
  static Future<bool> postClinicalRegistration(
      ClientIntake client, BuildContext context) async {
    try {
      String url = endPointBaseUrl + '/postclinicalregistration';
      http.Response response = await RequestMiddleWare.makeRequest(
          url: url,
          method: RequestMethod.POST,
          body: JsonEncoder().convert([
            client.isRegisteredOnline == true
                ? client.toJsonUpdate(context)
                : client.toJson(context)
          ]),
          headers: {'Content-Type': 'application/json'});
      if (response.statusCode == 200) {
        List data = JsonDecoder().convert(response.body);
        print(data);
        if (client.isRegisteredOnline == false) {
          ClientsDB.getInstance().deleteClient(client.clientCode);
        }
        return true;
      }
      print(response.body);
      throw ('Error saving data');
    } on SocketException catch (_) {
      throw ('Connection Error');
    }
  }

  static Future<void> postClinicalRegistrationBulk(
      List<ClientIntake> clients, BuildContext context) async {
    try {
      String url = endPointBaseUrl + '/postclinicalregistration';
      http.Response response = await RequestMiddleWare.makeRequest(
          method: RequestMethod.POST,
          url: url,
          body: JsonEncoder().convert(clients.map((e) {
            return e.toJson(context);
          }).toList()),
          headers: {'Content-Type': 'application/json'});
      if (response.statusCode == 200) {
        List data = JsonDecoder().convert(response.body);

        clients.forEach((client) async {
          Map json;
          data.forEach((element) {
            if (client.hospitalNum == element['hospitalNum']) {
              json = element;
            }
          });

          if (json != null) {
            await ClientsDB.getInstance().deleteClient(client.clientCode);
            // int idTemp = json['userId'];
            // String uuidTemp = json['idUuid'];
            // ClientsDB.getInstance().updateClient(
            //     client.clientCode,
            //     {
            //       'user_id': idTemp,
            //       'uuid': uuidTemp,
            //       'is_registered_online': true
            //     },
            //     context);
          }
        });
      }
    } on SocketException catch (_) {
      throw ('Connection Error');
    }
  }
}
