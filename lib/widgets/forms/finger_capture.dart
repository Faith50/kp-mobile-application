import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kp/util.dart';
import 'package:kp/widgets/finger_capture_dialog.dart';
import 'package:kp/widgets/forms/form_template.dart';

class FingerCaptureForm extends StatefulWidget {
  final void Function(String leftThumb, String rightThumb) onFinished;
  final int stepIndex;
  final int numberOfSteps;
  final bool disableBackButton;
  final bool disableForwardButton;
  final VoidCallback onBack;
  FingerCaptureForm(
      {this.stepIndex,
      this.onFinished,
      this.numberOfSteps,
      this.onBack,
      this.disableBackButton,
      this.disableForwardButton});

  @override
  State createState() => FingerCaptureFormState();
}

class FingerCaptureFormState extends State<FingerCaptureForm> {
  String leftThumb;
  String rightThumb;
  String leftIndex;
  String rightIndex;
  String leftMid;
  String rightMid;

  @override
  Widget build(BuildContext context) {
    return FormTemplate(
      onFinished: () {
        FocusScope.of(context).requestFocus(new FocusNode());

        //temprarily diasbale finger capture
        return widget.onFinished('', '');

        if (widget.onFinished != null) {
          if (leftThumb == null || rightThumb == null) {
            showBasicMessageDialog("Finger Capture incomplete", context);
          } else {
            print(leftThumb);
            widget.onFinished(leftThumb.split('/').last.split('.').first,
                rightThumb.split('/').last.split('.').first);
          }
        }
      },
      stepIndex: widget.stepIndex,
      numberOfSteps: widget.numberOfSteps,
      disableBackButton: widget.disableBackButton,
      disableForwardButton: widget.disableForwardButton,
      onBack: widget.onBack,
      nextIsSave: false,
      title: "Fingerprint Capture",
      children: [
        Center(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(child: Container()),
                  Expanded(child: Container()),
                  Expanded(
                      child: FittedBox(
                    child: Finger(
                      value: leftThumb,
                      label: "Left Thumb",
                      onCaptured: (path) {
                        setState(() {
                          leftThumb = path;
                        });
                      },
                    ),
                    fit: BoxFit.fitWidth,
                  )),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                      child: FittedBox(
                    child: Finger(
                      value: rightThumb,
                      label: "Right Thumb",
                      onCaptured: (path) {
                        setState(() {
                          rightThumb = path;
                        });
                      },
                    ),
                    fit: BoxFit.fitWidth,
                  )),
                  Expanded(child: Container()),
                  Expanded(child: Container()),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(child: Container()),
                  Expanded(
                      child: FittedBox(
                    child: Finger(
                      value: leftIndex,
                      label: "Left Index",
                      onCaptured: (path) {
                        setState(() {
                          leftIndex = path;
                        });
                      },
                    ),
                    fit: BoxFit.fitWidth,
                  )),
                  Expanded(child: Container()),
                  Expanded(child: Container()),
                  Expanded(
                      child: FittedBox(
                    child: Finger(
                      value: rightIndex,
                      label: "Right Index",
                      onCaptured: (path) {
                        setState(() {
                          rightIndex = path;
                        });
                      },
                    ),
                    fit: BoxFit.fitWidth,
                  )),
                  Expanded(child: Container()),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                      child: FittedBox(
                    child: Finger(
                      value: leftMid,
                      label: "Left Mid",
                      onCaptured: (path) {
                        setState(() {
                          leftMid = path;
                        });
                      },
                    ),
                    fit: BoxFit.fitWidth,
                  )),
                  Expanded(child: Container()),
                  Expanded(child: Container()),
                  Expanded(child: Container()),
                  Expanded(child: Container()),
                  Expanded(
                      child: FittedBox(
                    child: Finger(
                      value: rightMid,
                      label: "Right Mid",
                      onCaptured: (path) {
                        setState(() {
                          rightMid = path;
                        });
                      },
                    ),
                    fit: BoxFit.fitWidth,
                  ))
                ],
              ),
              SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class Finger extends StatefulWidget {
  final Function(String path) onCaptured;
  final String value;
  final String label;
  Finger({this.onCaptured, this.value, this.label});
  @override
  State createState() => FingerState();
}

class FingerState extends State<Finger> {
  Future<String> captureFinger() async {
    bool capture = true;
    if (widget.value != null) {
      capture = await showBasicConfirmationDialog(
          "Already captured this finger. Do you want to recapture?", context);
    }
    if (capture) {
      return await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              elevation: 10,
              contentPadding: EdgeInsets.zero,
              insetPadding: EdgeInsets.zero,
              backgroundColor: Colors.transparent,
              content: FingerCaptureDialog(),
            );
          });
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        String path = await captureFinger();
        if (path != null) {
          if (widget.onCaptured != null) {
            widget.onCaptured(path);
          }
        }
      },
      child: Container(
        height: 150,
        width: 150,
        decoration:
            BoxDecoration(border: Border.all(color: Colors.black, width: 3)),
        child: widget.value != null
            ? Image.file(new File(widget.value))
            : Center(
                child: Text(
                  widget.label,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
      ),
    );
  }
}
