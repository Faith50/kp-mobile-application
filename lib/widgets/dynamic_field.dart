//textarea
//textfield
//number
//password

import 'package:flutter/material.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:provider/provider.dart';

class DynamicField extends StatelessWidget {
  final Map<String, dynamic> config;
  DynamicField({this.config});
  @override
  Widget build(BuildContext context) {
    final InputBorder border = OutlineInputBorder(
        // borderRadius: BorderRadius.circular(10),
        borderSide: BorderSide(width: 0, color: Colors.grey[100]),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(config['prefix'] != null ? 0 : 10),
            bottomLeft: Radius.circular(config['prefix'] != null ? 0 : 10),
            topRight: Radius.circular(config['suffix'] != null ? 0 : 10),
            bottomRight: Radius.circular(config['suffix'] != null ? 0 : 10)));
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        config['label'] != null
            ? Padding(
                padding: EdgeInsets.only(left: 5),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(config['label'],
                        style: TextStyle(fontWeight: FontWeight.w500)),
                    SizedBox(
                      height: 7,
                    ),
                  ],
                ),
              )
            : Container(),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            config['prefix'] != null
                ? Container(
                    decoration: BoxDecoration(color: Colors.grey[300]),
                    height: kToolbarHeight + 3,
                    padding: EdgeInsets.all(10),
                    child: Center(
                      child: Text(config['prefix'].toString()),
                    ),
                  )
                : Container(),
            Expanded(
                child: TextFormField(
              onChanged: (val) {
                Provider.of<AuthProvider>(context, listen: false)
                    .resetInactivityTimer();
              },
              keyboardType: config['type'] == 'number'
                  ? TextInputType.number
                  : TextInputType.text,
              style: TextStyle(fontSize: 15),
              enabled: config['disabled'] == null ? true : !config['disabled'],
              decoration: InputDecoration(
                  fillColor: Colors.grey[100],
                  filled: true,
                  border: border,
                  focusedBorder: border,
                  hintText: config['placeholder'] == null
                      ? ''
                      : config['placeholder'],
                  enabledBorder: border),
              obscureText:
                  config['protected'] == null ? false : config['protected'],
              maxLines: config['type'] == 'textarea'
                  ? config['rows'] == null
                      ? 3
                      : config['rows']
                  : 1,
            )),
            config['suffix'] != null
                ? Container(
                    decoration: BoxDecoration(color: Colors.grey[300]),
                    height: kToolbarHeight + 3,
                    padding: EdgeInsets.all(10),
                    child: Center(
                      child: Text(config['suffix'].toString()),
                    ),
                  )
                : Container(),
          ],
        ),
        config['description'] != null
            ? Padding(
                padding: EdgeInsets.only(left: 5),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 7,
                    ),
                    Text(config['description'],
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.grey)),
                  ],
                ),
              )
            : Container(),
      ],
    );
  }
}
