//checkbox
//radio

import 'package:flutter/material.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:provider/provider.dart';

class DynamicSelectBox extends StatefulWidget {
  final Map config;
  DynamicSelectBox({this.config});
  @override
  State createState() => DynamicSelectBoxState();
}

class DynamicSelectBoxState extends State<DynamicSelectBox> {
  bool selected = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            widget.config['type'] == 'checkbox' ||
                    widget.config['type'] == 'radio'
                ? Checkbox(
                    value: selected,
                    onChanged: widget.config['disabled'] == true
                        ? null
                        : (val) {
                            Provider.of<AuthProvider>(context, listen: false)
                                .resetInactivityTimer();
                            setState(() {
                              selected = val;
                            });
                          },
                  )
                : Container(),
            Text(widget.config['label'] == null ? '' : widget.config['label'])
          ],
        ),
        widget.config['description'] != null
            ? Padding(
                padding: EdgeInsets.only(left: 15),
                child: Text(
                  widget.config['description'],
                  style: TextStyle(color: Colors.grey),
                ),
              )
            : Container()
      ],
    );
  }
}
