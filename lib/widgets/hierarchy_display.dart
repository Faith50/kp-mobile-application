import 'package:flutter/material.dart';
import 'package:kp/models/heirachy_unit.dart';
import 'package:kp/models/metadata.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:kp/providers/meta_provider.dart';
import 'package:kp/widgets/labelled_text_field.dart';
import 'package:kp/widgets/section_header.dart';
import 'package:provider/provider.dart';

class HierarchyDisplay extends StatefulWidget {
  final void Function(String path) onChanged;
  final String initialPath;
  HierarchyDisplay({this.onChanged, this.initialPath});
  @override
  State createState() => HierarchyDisplayState();
}

class HierarchyDisplayState extends State<HierarchyDisplay> {
  String path;
  HierarchyUnit selectedUnit;

  @override
  void initState() {
    path = widget.initialPath;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<HierarchyUnit> hierarchyUnits = context
        .select((MetadataProvider metaProvider) => metaProvider.hierarchyUnits);
    if (hierarchyUnits != null) {
      hierarchyUnits.removeWhere((element) => element.id == 431);
    }
    if (hierarchyUnits == null) {
      return LabeledTextField(
        readOnly: true,
        controller: TextEditingController(text: 'Loading List..'),
        text: "Facility Name",
        onTap: () {},
      );
    }
    List<Widget> children = [];
    children = hierarchyUnits.map((e) {
      return HierarchyUnitDisplay(
        unit: e,
        level: 1,
        onSelected: (val, unit) {
          setState(() {
            path = val;
            selectedUnit = unit;
            print(val);
            if (unit != null) {
              print(unit.name);
            }
          });
          widget.onChanged(val);
        },
      );
    }).toList();
    return LabeledTextField(
      readOnly: true,
      controller: TextEditingController(
          text: selectedUnit == null ? "" : selectedUnit.name),
      text: "Facility Name",
      onTap: () {
        Provider.of<AuthProvider>(context, listen: false)
            .resetInactivityTimer();
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: SectionHeader(
                  text: 'Facility List',
                ),
                content: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.7,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    children: [
                      Container(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [...children],
                        ),
                      )
                    ],
                  ),
                ),
                actions: [
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'OKAY',
                        style: TextStyle(color: Colors.blueAccent),
                      ))
                ],
              );
            });
      },
    );
  }
}

class HierarchyUnitDisplay extends StatefulWidget {
  final HierarchyUnit unit;
  final int level;
  final String groupValue;
  final void Function(String value, HierarchyUnit unit) onSelected;
  HierarchyUnitDisplay({
    this.unit,
    this.level,
    this.groupValue,
    this.onSelected,
  });
  @override
  State createState() => HierarchyUnitDisplayState();
}

class HierarchyUnitDisplayState extends State<HierarchyUnitDisplay> {
  bool expanded = false;
  String selected;

  @override
  void initState() {
    if (widget.groupValue == null) {
      selected = widget.unit.code;
      if (selected == null) {
        selected = '';
      }
    } else {
      selected = widget.groupValue + "/${widget.unit.code}";
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];
    if (expanded) {
      children = widget.unit.children.map((e) {
        return HierarchyUnitDisplay(
          unit: e,
          level: widget.level + 1,
          groupValue: selected,
          onSelected: (val, unit) {
            setState(() {
              selected = val;
              widget.onSelected(val, unit);
            });
          },
        );
      }).toList();
    }
    return Container(
      child: Column(
        children: [
          InkWell(
            onTap: () {
              Provider.of<AuthProvider>(context, listen: false)
                  .resetInactivityTimer();
              setState(() {
                expanded = !expanded;
              });
            },
            child: Row(
              children: [
                widget.level > 3 && widget.unit.children.length == 0
                    ? Checkbox(
                        value: widget.groupValue.split('/').last ==
                            widget.unit.code,
                        onChanged: (val) {
                          if (widget.unit.code == null) {
                            print('code is null');
                            return;
                          }
                          if (widget.onSelected != null) {
                            String path;
                            HierarchyUnit un;
                            if (val == true) {
                              if (widget.groupValue.split('/').length ==
                                  widget.level) {
                                List<String> temp =
                                    widget.groupValue.split('/');
                                temp.removeLast();
                                path = temp.join('/') + "/" + widget.unit.code;
                              } else {
                                path =
                                    widget.groupValue + "/" + widget.unit.code;
                              }
                              un = widget.unit;
                            } else {
                              List<String> temp = widget.groupValue.split('/');
                              temp.removeLast();
                              path = temp.join('/');
                              un = null;
                            }
                            widget.onSelected(path, un);
                          }
                        })
                    : Icon(expanded == true
                        ? Icons.keyboard_arrow_up
                        : Icons.keyboard_arrow_down_rounded),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    child: Text(
                  widget.unit.name,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ))
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          expanded
              ? Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Column(
                    children: [...children],
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
