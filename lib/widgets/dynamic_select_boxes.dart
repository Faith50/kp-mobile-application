//selectboxes

import 'package:flutter/material.dart';
import 'package:kp/widgets/dynamic_select_box.dart';

class DynamicSelectBoxes extends StatefulWidget {
  final Map<String, dynamic> config;
  DynamicSelectBoxes({this.config});
  @override
  State createState() => DynamicSelectBoxesState();
}

class DynamicSelectBoxesState extends State<DynamicSelectBoxes> {
  @override
  Widget build(BuildContext context) {
    List values = widget.config['values'];
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.config['label'] != null
            ? Padding(
                padding: EdgeInsets.only(left: 15),
                child: Text(widget.config['label']),
              )
            : Container(),
        ...values.map((e) {
          return DynamicSelectBox(
            config: {
              'type': 'checkbox',
              'label': e['label'],
              'value': e['value']
            },
          );
        }).toList(),
        widget.config['description'] != null
            ? Padding(
                child: Text(
                  widget.config['description'],
                  style: TextStyle(color: Colors.grey),
                ),
                padding: EdgeInsets.only(left: 15),
              )
            : Container(),
      ],
    );
  }
}
