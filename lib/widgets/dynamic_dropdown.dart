//select

import 'package:flutter/material.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:kp/providers/meta_provider.dart';
import 'package:kp/widgets/dynamic_select_box.dart';
import 'package:provider/provider.dart';

class DynamicDropdown extends StatefulWidget {
  final Map<String, dynamic> config;
  DynamicDropdown({this.config});
  @override
  State createState() => DynamicDropdownState();
}

class DynamicDropdownState extends State<DynamicDropdown> {
  var selected;
  @override
  Widget build(BuildContext context) {
    List values = widget.config['data']['values'];
    String url = widget.config['data']['url'];
    return Consumer<MetadataProvider>(
      builder: (context, metaProvider, _) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            widget.config['label'] != null
                ? Padding(
                    padding: EdgeInsets.only(left: 5),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(widget.config['label'],
                            style: TextStyle(fontWeight: FontWeight.w500)),
                        SizedBox(
                          height: 7,
                        ),
                      ],
                    ),
                  )
                : Container(),
            Container(
              padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
              decoration: BoxDecoration(
                  color: Colors.grey[100],
                  borderRadius: BorderRadius.circular(5)),
              child: DropdownButton(
                onTap: () {
                  Provider.of<AuthProvider>(context, listen: false)
                      .resetInactivityTimer();
                },
                isExpanded: true,
                underline: Container(),
                items: url != null
                    ? metaProvider.getMetaFromString(url) == null
                        ? null
                        : metaProvider.getMetaFromString(url).map((e) {
                            return DropdownMenuItem(
                              child: Text(e.name),
                              value: e,
                            );
                          }).toList()
                    : values == null
                        ? null
                        : values.map((e) {
                            return DropdownMenuItem(
                              child: Text(e['label'].toString()),
                              value: e['value'],
                            );
                          }).toList(),
                onChanged: (val) {
                  Provider.of<AuthProvider>(context, listen: false)
                      .resetInactivityTimer();
                  setState(() {
                    selected = val;
                  });
                },
                value: selected,
              ),
            ),
            widget.config['description'] != null
                ? Padding(
                    padding: EdgeInsets.only(left: 5),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(widget.config['description'],
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.grey)),
                        SizedBox(
                          height: 7,
                        ),
                      ],
                    ),
                  )
                : Container(),
          ],
        );
      },
    );
  }
}
