//radio

import 'package:flutter/material.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:provider/provider.dart';

class DynamicRadio extends StatefulWidget {
  final Map<String, dynamic> config;
  DynamicRadio({this.config});
  @override
  State createState() => DynamicRadioState();
}

class DynamicRadioState extends State<DynamicRadio> {
  var groupValue;
  @override
  Widget build(BuildContext context) {
    List values = widget.config['values'];
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.config['label'] != null
            ? Padding(
                padding: EdgeInsets.only(left: 15),
                child: Text(widget.config['label']),
              )
            : Container(),
        ...values.map((e) {
          return Row(
            children: [
              Radio(
                  value: e['value'],
                  groupValue: groupValue,
                  onChanged: (val) {
                    Provider.of<AuthProvider>(context, listen: false)
                        .resetInactivityTimer();
                    setState(() {
                      groupValue = val;
                    });
                  }),
              Text(e['label'])
            ],
          );
        }).toList(),
        widget.config['description'] != null
            ? Padding(
                child: Text(
                  widget.config['description'],
                  style: TextStyle(color: Colors.grey),
                ),
                padding: EdgeInsets.only(left: 15),
              )
            : Container(),
      ],
    );
  }
}
