import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:kp/util.dart';
import 'package:kp/widgets/dynamic_add_another.dart';
import 'package:kp/widgets/dynamic_date_time_picker.dart';
import 'package:kp/widgets/dynamic_dropdown.dart';
import 'package:kp/widgets/dynamic_field.dart';
import 'package:kp/widgets/dynamic_radio.dart';
import 'package:kp/widgets/dynamic_select_box.dart';
import 'package:kp/widgets/dynamic_select_boxes.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class DynamicForm extends StatefulWidget {
  final Map<String, dynamic> config;
  DynamicForm({this.config});
  @override
  State createState() => DynamicFormState();
}

class DynamicFormState extends State<DynamicForm> {
  ScrollController controller = ScrollController();

  @override
  void initState() {
    controller.addListener(() {
      Provider.of<AuthProvider>(context, listen: false).resetInactivityTimer();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(JsonEncoder().convert(widget.config));
    return Container(
      color: Colors.white,
      child: ResponsiveBuilder(
        builder: (context, sizingInfo) {
          List components = widget.config['components'];
          if (components == null) {
            return Center(
              child: Text(
                'The form contains no elements',
                style: TextStyle(color: Colors.blueAccent, fontSize: 15),
              ),
            );
          }

          List<Widget> fields = [];
          components.forEach((e) {
            addFormComponents(fields, e, sizingInfo);
          });

          int chunk;
          if (sizingInfo.isMobile) {
            chunk = 2;
          } else {
            chunk = 3;
          }

          List<Widget> rows = splitToChunks(fields, chunk);
          return ResponsiveBuilder(
            builder: (context, sizingInfo) {
              return Padding(
                padding: EdgeInsets.all(15),
                child: ListView(
                  controller: controller,
                  padding: EdgeInsets.zero,
                  children: [
                    ...rows,
                    sizingInfo.isMobile == true
                        ? Padding(
                            padding:
                                EdgeInsets.only(left: 5, right: 5, top: 30),
                            child: SizedBox(
                              height: 55,
                              child: RaisedButton(
                                onPressed: () {},
                                child: Text('Submit',
                                    style: TextStyle(color: Colors.white)),
                                color: Colors.blueAccent,
                              ),
                            ),
                          )
                        : Container()
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}

void addFormComponents(
    List<Widget> fields, Map<String, dynamic> e, SizingInformation sizingInfo) {
  Widget component;
  if (e['columns'] != null) {
    e['columns'].forEach((val) {
      addFormComponents(fields, val, sizingInfo);
    });
  } else if (e['addAnother'] != null) {
    List<Widget> f = [];
    e['components'].forEach((val) {
      addFormComponents(f, val, sizingInfo);
    });
    int chunk;
    if (sizingInfo.isMobile) {
      chunk = 2;
    } else {
      chunk = 3;
    }
    List<Widget> rows = splitToChunks(f, chunk);
    component = DynamicAddAnother(
      child: Column(
        children: [...rows],
      ),
      addText: e['addAnother'],
    );
  } else if (e['components'] != null) {
    e['components'].forEach((val) {
      addFormComponents(fields, val, sizingInfo);
    });
  } else if (e['type'] == 'textfield' ||
      e['type'] == 'textarea' ||
      e['type'] == 'number' ||
      e['type'] == 'password') {
    component = DynamicField(
      config: e,
    );
  } else if (e['type'] == 'select') {
    component = DynamicDropdown(
      config: e,
    );
  } else if (e['type'] == 'radio') {
    if (e['values'] != null) {
      component = DynamicRadio(
        config: e,
      );
    } else {
      component = DynamicSelectBox(
        config: e,
      );
    }
  } else if (e['type'] == 'checkbox') {
    component = DynamicSelectBox(
      config: e,
    );
  } else if (e['type'] == 'selectboxes') {
    component = DynamicSelectBoxes(
      config: e,
    );
  } else if (e['type'] == 'datetime') {
    component = DynamicDateTimePicker(
      config: e,
    );
  } else {
    component = null;
  }
  if (component != null) {
    fields.add(component);
  }
}
