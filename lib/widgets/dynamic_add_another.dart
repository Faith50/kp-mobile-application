import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DynamicAddAnother extends StatefulWidget {
  final Widget child;
  final String addText;
  DynamicAddAnother({this.child, this.addText});
  @override
  DynamicAddAnotherState createState() => DynamicAddAnotherState();
}

class DynamicAddAnotherState extends State<DynamicAddAnother> {
  List<Widget> children;

  @override
  void initState() {
    children = [widget.child];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> c = children.map((e) {
      return Column(
        children: [e, Divider()],
      );
    }).toList();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...c,
        SizedBox(
          height: 15,
        ),
        RaisedButton(
          color: Colors.blueAccent,
          onPressed: () {
            setState(() {
              children.add(widget.child);
            });
          },
          child: Text(widget.addText),
        )
      ],
    );
  }
}
