import 'package:flutter/cupertino.dart';

String endPointBaseUrl = 'http://198.38.91.113:4040/api/v1';
// String endPointBaseUrl = 'http://13.59.32.70:4040/api/v1';
// String endPointBaseUrl = 'http://192.168.1.179:4040/api/v1';
BuildContext globalBuildContext;

Map<String, Map<String, dynamic>> genericMetaDataList = {
  'genders': {'id_name': 'gender_id', 'end_point': '/listgenders'},
  'religions': {'id_name': 'religion_id', 'end_point': '/listreligions'},
  'maritalstatus': {
    'id_name': 'marital_status_id',
    'end_point': '/listmaritalstatus'
  },
  'occupations': {'id_name': 'occupation_id', 'end_point': '/listoccupations'},
  'allergies': {'id_name': 'allergy_id', 'end_point': '/listallergies'},
  'allergens': {'id_name': 'allergen_id', 'end_point': '/listallergens'},
  'disabilities': {
    'id_name': 'disability_id',
    'end_point': '/listdisabilities'
  },
  'targetgroups': {
    'id_name': 'target_group_id',
    'end_point': '/listtargetgroups'
  },
  'careentrypoint': {
    'id_name': 'care_entry_point_id',
    'end_point': '/listcareentrypoint'
  },
  'priorart': {'id_name': 'prior_art_id', 'end_point': '/listpriorart'},
  'referredfrom': {
    'id_name': 'referred_from_id',
    'end_point': '/listreferredfrom'
  },
  'languages': {'id_name': 'language_id', 'end_point': '/listlanguages'},
  'qualifications': {
    'id_name': 'qualification_id',
    'end_point': '/listqualifications'
  },
  'drugs': {'id_name': 'drug_id', 'end_point': '/listdrugs'},
  'drugfrequency': {
    'id_name': 'drug_frequency_id',
    'end_point': '/listdrugfrequency'
  },
  'drugunits': {'id_name': 'drug_unit_id', 'end_point': '/listdrugunits'},
  'nationalities': {
    'id_name': 'nationality_id',
    'end_point': '/listnationalities'
  },
  'severity': {'id_name': 'severity_id', 'end_point': '/listseverity'},
  'diagnosisconditions': {
    'id_name': 'diagnosis_condition_id',
    'end_point': '/listdiagnosisconditions'
  },
  'relationships': {
    'id_name': 'relationship_id',
    'end_point': '/listrelationships'
  },
};
