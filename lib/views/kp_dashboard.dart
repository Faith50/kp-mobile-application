import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kp/views/Clinical/appointment.dart';
import 'package:kp/views/Clinical/vital_signs.dart';
import 'package:kp/views/Clinical/allergies.dart';

void main() => runApp(new Dashboard());

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: <Widget>[
      new Padding(
        padding: const EdgeInsets.only(top: 40),
      ),
      GridView.count(
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          primary: false,
          //padding: EdgeInsets.only(top: 180),
          children: <Widget>[
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              elevation: 15,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.hospitalUser,
                    ),
                    iconSize: 60,
                    color: Color(0xff011829),
                    onPressed: () => {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Appointment()))
                    },
                  ),
                  Text('Patient',
                      style: TextStyle(
                        color: Color(0xff011829),
                        fontSize: 18.0,
                        fontFamily: "Montserrat Regular",
                      )),
                ],
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              elevation: 15,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.allergies,
                    ),
                    iconSize: 80,
                    color: Color(0xffff2400),
                    onPressed: () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Allergies()))
                    },
                  ),
                  Text('Pharmacy',
                      style: TextStyle(
                        color: Color(0xffff2400),
                        fontSize: 18.0,
                        fontFamily: "Montserrat Regular",
                      )),
                ],
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              elevation: 15,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        FontAwesomeIcons.heartbeat,
                      ),
                      iconSize: 80,
                      color: Color(0xffff0000),
                      onPressed: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VitalSign()))
                      },
                    ),
                    Text('Laboratory',
                        style: TextStyle(
                          color: Color(0xffff0000),
                          fontSize: 18.0,
                          fontFamily: "Montserrat Regular",
                        )),
                  ]),
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              elevation: 15,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.stethoscope,
                    ),
                    iconSize: 80,
                    color: Color(0xff008f6f),
                    onPressed: () => {
                      //Navigator.push(context,
                      // MaterialPageRoute(builder: (context) => Allergies()))
                    },
                  ),
                  Text('Diagnosis',
                      style: TextStyle(
                        color: Color(0xff008f6f),
                        fontSize: 18.0,
                        fontFamily: "Montserrat Regular",
                      )),
                ],
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              elevation: 15,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.thermometerThreeQuarters,
                    ),
                    iconSize: 80,
                    color: Color(0xff800020),
                    onPressed: () => {
                      //Navigator.push(context,
                      // MaterialPageRoute(builder: (context) => Allergies()))
                    },
                  ),
                  Text('Laboratory Test',
                      style: TextStyle(
                        color: Color(0xff800020),
                        fontSize: 18.0,
                        fontFamily: "Montserrat Regular",
                      )),
                ],
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              elevation: 15,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.briefcaseMedical,
                    ),
                    iconSize: 80,
                    color: Color(0xff000080),
                    onPressed: () => {
                      //Navigator.push(context,
                      // MaterialPageRoute(builder: (context) => Allergies()))
                    },
                  ),
                  Text('Pharmacy',
                      style: TextStyle(
                        color: Color(0xff000080),
                        fontSize: 18.0,
                        fontFamily: "Montserrat Regular",
                      )),
                ],
              ),
            )
          ],
          crossAxisCount: 2),
    ]));
  }
}
