import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:kp/api/clients_api.dart';
import 'package:kp/api/request_middleware.dart';
import 'package:kp/db/clients.dart';
import 'package:kp/globals.dart';
import 'package:kp/models/client_intake_model.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:kp/util.dart';
import 'package:kp/views/patient_details.dart';
import 'package:kp/widgets/custom_text_field.dart';
import 'package:kp/widgets/patient_tile.dart';
import 'package:provider/provider.dart';
import 'package:sembast/sembast.dart';
import 'package:http/http.dart' as http;

class PatientsView extends StatefulWidget {
  @override
  State createState() => PatientsViewState();
}

class PatientsViewState extends State<PatientsView> {
  List<ClientIntake> offlineClients;
  StreamSubscription subscription;
  bool showSearchingModal = false;
  TextEditingController controller = new TextEditingController();
  String selectedView = "online";
  ScrollController scrollController = ScrollController();

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    controller.addListener(() {
      Provider.of<AuthProvider>(context, listen: false).resetInactivityTimer();
    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      ClientsDB.getInstance().getAllClientsSnapshot().then((value) {
        if (value != null) {
          subscription = value.listen((event) {
            List<RecordSnapshot<int, Map<String, dynamic>>> clients = event;
            List<ClientIntake> temp = [];
            clients.forEach((recordSnap) {
              print(recordSnap.value);
              temp.add(ClientIntake.fromDBJson(recordSnap.value));
            });
            setState(() {
              this.offlineClients = temp;
            });
          });
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraint) {
      return Container(
        color: Colors.white,
        height: constraint.maxHeight,
        width: constraint.maxWidth,
        child: DefaultTabController(
          length: 2,
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Container(
                  height: 40,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: DropdownButton<String>(
                            underline: Container(),
                            items: [
                              DropdownMenuItem(
                                  child: Text('Online Clients',
                                      style:
                                          TextStyle(color: Colors.blueAccent)),
                                  value: 'online'),
                              DropdownMenuItem(
                                child: Text('Pending Client Uploads',
                                    style: TextStyle(color: Colors.blueAccent)),
                                value: 'offline',
                              ),
                            ],
                            onChanged: (selected) {
                              if (selectedView == selected) {
                                return;
                              }
                              setState(() {
                                selectedView = selected;
                              });
                            },
                            value: selectedView,
                            // dropdownColor: Colors.blueAccent,
                            elevation: 5,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                selectedView == "online"
                    ? Expanded(child: OnlinePatients())
                    : Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                    child: CustomTextField(
                                  controller: controller,
                                  onChanged: (val) {
                                    if (val.trim().length == 0) {
                                      ClientsDB.getInstance()
                                          .getAllClients()
                                          .then((clients) {
                                        setState(() {
                                          this.offlineClients = clients;
                                        });
                                      });
                                    } else {
                                      ClientsDB.getInstance()
                                          .searchClient(val)
                                          .then((value) {
                                        setState(() {
                                          this.offlineClients = value;
                                        });
                                      });
                                    }
                                  },
                                )),
                                // SizedBox(
                                //   width: 10,
                                // ),
                                // GestureDetector(
                                //   onTap: () {
                                //     VeriFingerSDK.findClient(
                                //             clientList: offlineClients,
                                //             context: context)
                                //         .then((client) {
                                //       if (client == null) {
                                //         return showBasicMessageDialog(
                                //             'No client found', context);
                                //       }
                                //       Navigator.push(
                                //           context,
                                //           MaterialPageRoute(
                                //               builder: (context) =>
                                //                   PatientDetails(
                                //                     client: client,
                                //                   )));
                                //     });
                                //   },
                                //   child: Icon(
                                //     Icons.fingerprint,
                                //     size: 40,
                                //   ),
                                // )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            RaisedButton(
                              onPressed: offlineClients == null
                                  ? null
                                  : offlineClients.length == 0
                                      ? null
                                      : () async {
                                          bool val =
                                              await showBasicConfirmationDialog(
                                                  "Are you sure you want to upload all registrations?",
                                                  context);
                                          if (val == true) {
                                            showPersistentLoadingIndicator(
                                                context);
                                            List<ClientIntake> clients =
                                                await ClientsDB.getInstance()
                                                    .getClients(Finder(
                                                        filter: Filter.equals(
                                                            'is_registered_online',
                                                            false)));
                                            await ClientApi
                                                .postClinicalRegistrationBulk(
                                                    clients, context);
                                            Navigator.pop(context);
                                          }
                                        },
                              child: Text(
                                'Upload All',
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Colors.blueAccent,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Expanded(
                                child: offlineClients == null
                                    ? Center(
                                        child: SizedBox(
                                          height: 25,
                                          width: 25,
                                          child: CircularProgressIndicator(),
                                        ),
                                      )
                                    : offlineClients.length == 0 &&
                                            controller.text.trim().length == 0
                                        ? Center(
                                            child:
                                                Text('Nothing to see here yet'),
                                          )
                                        : offlineClients.length == 0 &&
                                                controller.text.trim().length >
                                                    0
                                            ? Center(
                                                child: Text(
                                                    'No patients matches the search parameter'),
                                              )
                                            : Container(
                                                child: ListView(
                                                  controller: scrollController,
                                                  physics:
                                                      BouncingScrollPhysics(),
                                                  children: [
                                                    TableBuilder(
                                                      clients: offlineClients,
                                                    )
                                                  ],
                                                ),
                                              ))
                          ],
                        ),
                      )
              ],
            ),
          ),
        ),
      );
    });
  }
}

class OnlinePatients extends StatefulWidget {
  final bool isSelect;
  OnlinePatients({this.isSelect = false});
  @override
  State createState() => OnlinePatientsState();
}

List<ClientIntake> savedClients = [];

class OnlinePatientsState extends State<OnlinePatients> {
  ScrollController scrollController = new ScrollController();
  TextEditingController controller = TextEditingController();
  List<ClientIntake> clients;
  List<ClientIntake> matchedClients;
  bool showSearchResult = false;
  bool fetchingClients = true;
  String orderBy = "name";

  @override
  void initState() {
    super.initState();
    fetchingClients = true;
    if (savedClients.length > 0) {
      getClients();
      clients = savedClients;
    }
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (savedClients.length == 0) {
        getClients();
      }
    });

    scrollController.addListener(() {
      Provider.of<AuthProvider>(context, listen: false).resetInactivityTimer();
    });
  }

  Future<void> getClients({String lastGameId}) async {
    print('getting clients');
    setState(() {
      fetchingClients = true;
    });
    try {
      http.Response response = await RequestMiddleWare.makeRequest(
          url: endPointBaseUrl + "/listclinicalregistrations",
          method: RequestMethod.GET);
      if (response.statusCode == 200) {
        List clientJsons = JsonDecoder().convert(response.body);
        List<ClientIntake> clients = clientJsons.map((json) {
          return ClientIntake.formServerJson(json);
        }).toList();
        this.clients = clients;
        savedClients = clients;
      }
      setState(() {
        fetchingClients = false;
      });
    } catch (e) {
      print('here');
      print(e);
      setState(() {
        fetchingClients = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextField(
          controller: controller,
          onChanged: (query) {
            if (query.trim().length == 0) {
              setState(() {
                showSearchResult = false;
              });
            } else {
              List<ClientIntake> matchedClients = [];
              this.clients.forEach((client) {
                try {
                  if (client.firstName
                      .toLowerCase()
                      .contains(query.trim().toLowerCase())) {
                    matchedClients.add(client);
                    return;
                  }
                } catch (_) {}

                try {
                  if (client.surname
                      .toLowerCase()
                      .contains(query.toLowerCase())) {
                    matchedClients.add(client);
                    return;
                  }
                } catch (_) {}

                try {
                  if (client.phone
                      .toLowerCase()
                      .contains(query.toLowerCase())) {
                    matchedClients.add(client);
                    return;
                  }
                } catch (_) {}

                try {
                  if (client.hospitalNum
                      .toLowerCase()
                      .contains(query.toLowerCase())) {
                    matchedClients.add(client);
                    return;
                  }
                } catch (_) {}
              });
              setState(() {
                this.matchedClients = matchedClients;
                showSearchResult = true;
              });
            }
          },
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
            child: Padding(
          padding: EdgeInsets.all(0),
          child: clients == null && fetchingClients == true
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : clients == null && fetchingClients == false
                  ? Center(
                      child: RaisedButton(
                        color: Colors.blueAccent,
                        onPressed: () {
                          getClients();
                        },
                        child: Text('Retry',
                            style: TextStyle(color: Colors.white)),
                      ),
                    )
                  : clients.length == 0
                      ? Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Text(
                              "Nothing to see here yet",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )
                      : showSearchResult == true
                          ? ListView(
                              controller: scrollController,
                              physics: BouncingScrollPhysics(),
                              children: [
                                TableBuilder(
                                  clients: matchedClients,
                                )
                              ],
                            )
                          : RefreshIndicator(
                              child: ListView(
                                controller: scrollController,
                                physics: BouncingScrollPhysics(),
                                children: [
                                  TableBuilder(
                                    isSelect: widget.isSelect,
                                    clients: clients,
                                  )
                                ],
                              ),
                              onRefresh: () async {
                                await getClients();
                              }),
        )),
      ],
    );
  }
}

class TableBuilder extends StatelessWidget {
  final bool isSelect;
  final List<ClientIntake> clients;
  TableBuilder({this.clients, this.isSelect});
  @override
  Widget build(BuildContext context) {
    List<DataRow> dataRows = clients.map((e) {
      Function onTap;
      if (isSelect == true) {
        onTap = () {
          Navigator.pop(context, e);
        };
      } else {
        onTap = () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PatientDetails(
                        client: e,
                      )));
        };
      }
      return DataRow(
          cells: isSelect == true
              ? [
                  DataCell(GestureDetector(
                    child: Text(e.hospitalNum),
                    onTap: onTap,
                  )),
                  DataCell(GestureDetector(
                    onTap: onTap,
                    child: Text(e.firstName + " " + e.surname),
                  )),
                  DataCell(
                    GestureDetector(
                      onTap: onTap,
                      child: Text(e.dob == null
                          ? ""
                          : convertDateOfBirthToAge(e.dob).toString()),
                    ),
                  ),
                  DataCell(GestureDetector(
                    onTap: onTap,
                    child: Text(e.phone),
                  )),
                ]
              : [
                  DataCell(ClientOptions(
                    isDashboard: true,
                    client: e,
                  )),
                  DataCell(GestureDetector(
                    child: Text(e.hospitalNum),
                    onTap: onTap,
                  )),
                  DataCell(GestureDetector(
                    onTap: onTap,
                    child: Text(e.firstName + " " + e.surname),
                  )),
                  DataCell(
                    GestureDetector(
                      onTap: onTap,
                      child: Text(e.dob == null
                          ? ""
                          : convertDateOfBirthToAge(e.dob).toString()),
                    ),
                  ),
                  DataCell(GestureDetector(
                    onTap: onTap,
                    child: Text(e.phone),
                  )),
                ]);
    }).toList();
    return SingleChildScrollView(
      child: isSelect == true
          ? DataTable(columns: [
              DataColumn(label: Text('Client Unique Id')),
              DataColumn(label: Text('Name')),
              DataColumn(label: Text('Age')),
              DataColumn(label: Text('Contact')),
            ], rows: dataRows)
          : DataTable(
              columns: [
                DataColumn(label: Text('')),
                DataColumn(label: Text('Client Unique Id')),
                DataColumn(label: Text('Name')),
                DataColumn(label: Text('Age')),
                DataColumn(label: Text('Contact')),
              ],
              rows: dataRows,
              // columnSpacing: 10,
            ),
      scrollDirection: Axis.horizontal,
    );
  }
}
