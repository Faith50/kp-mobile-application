import 'dart:math';
import 'package:flutter/material.dart';
import 'package:kp/models/client_intake_model.dart';
import 'package:kp/widgets/custom_date_selector.dart';
import 'package:kp/widgets/labelled_text_field.dart';
import 'package:kp/widgets/section_header.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:kp/widgets/custom_time_picker.dart';
import 'package:kp/views/patients.dart' as patients;

class AppointmentView extends StatefulWidget {
  final ClientIntake client;
  AppointmentView({this.client});
  @override
  State createState() => AppointmentViewState();
}

class AppointmentViewState extends State<AppointmentView> {
  _DataSource events;
  List<Appointment> appointments;
  List<Color> colorCollection;
  Appointment _selectedAppointment;
  CalendarView calendarView = CalendarView.schedule;

  @override
  void initState() {
    appointments = <Appointment>[];
    addAppointmentDetails();
    addAppointments();
    events = _DataSource(appointments);
    super.initState();
  }

  void _onCalendarTapped(CalendarTapDetails calendarTapDetails) {
    /// Condition added to open the editor, when the calendar elements tapped
    /// other than the header.
    if (calendarTapDetails.targetElement == CalendarElement.header ||
        calendarTapDetails.targetElement == CalendarElement.viewHeader ||
        calendarTapDetails.targetElement == CalendarElement.resourceHeader) {
      return;
    }

    _selectedAppointment = null;

    if (calendarTapDetails.appointments != null &&
        calendarTapDetails.targetElement == CalendarElement.appointment) {
      _selectedAppointment = calendarTapDetails.appointments[0];
    }

    final DateTime selectedDate = calendarTapDetails.date;
    final CalendarElement targetElement = calendarTapDetails.targetElement;

    /// Navigates to the appointment editor page
    // Navigator.push<Widget>(
    //   context,
    //   MaterialPageRoute(builder: (BuildContext context) => Scaffold()),
    // );
  }

  void addAppointmentDetails() {
    colorCollection = <Color>[];
    colorCollection.add(const Color(0xFF0F8644));
    colorCollection.add(const Color(0xFF8B1FA9));
    colorCollection.add(const Color(0xFFD20100));
    colorCollection.add(const Color(0xFFFC571D));
    colorCollection.add(const Color(0xFF36B37B));
    colorCollection.add(const Color(0xFF01A1EF));
    colorCollection.add(const Color(0xFF3D4FB5));
    colorCollection.add(const Color(0xFFE47C73));
    colorCollection.add(const Color(0xFF636363));
    colorCollection.add(const Color(0xFF0A8043));
  }

  void addAppointments() {
    final Random random = Random();
    final DateTime rangeStartDate =
        DateTime.now().add(const Duration(days: -(365 ~/ 2)));
    final DateTime rangeEndDate = DateTime.now().add(const Duration(days: 365));
    for (DateTime i = rangeStartDate;
        i.isBefore(rangeEndDate);
        i = i.add(Duration(days: random.nextInt(10)))) {
      final DateTime date = i;
      final int count = 1 + random.nextInt(3);
      for (int j = 0; j < count; j++) {
        final DateTime startDate = DateTime(
            date.year, date.month, date.day, 8 + random.nextInt(8), 0, 0);
        appointments.add(Appointment(
          subject: "Client Appointment",
          startTime: startDate,
          endTime: startDate.add(Duration(hours: random.nextInt(3))),
          color: colorCollection[random.nextInt(9)],
          isAllDay: false,
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                TextEditingController clientController =
                    TextEditingController();
                if (widget.client != null) {
                  clientController.text = widget.client.hospitalNum;
                }
                return AlertDialog(
                  contentPadding: EdgeInsets.all(10),
                  title: SectionHeader(
                    text: "Schedule Appointment",
                  ),
                  content: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        LabeledTextField(
                          readOnly: true,
                          controller: clientController,
                          text: "Select Client",
                          onTap: () async {
                            if (widget.client == null) {
                              ClientIntake client = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Scaffold(
                                            appBar: AppBar(
                                              iconTheme: IconThemeData(
                                                  color: Colors.white),
                                              title: Text('Select Client'),
                                              backgroundColor:
                                                  Colors.blueAccent,
                                            ),
                                            body: Padding(
                                              padding: EdgeInsets.all(10),
                                              child: patients.OnlinePatients(
                                                isSelect: true,
                                              ),
                                            ),
                                          )));
                              if (client != null) {
                                print(client.hospitalNum);
                                clientController.text = client.hospitalNum;
                              }
                            }
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        LabeledTextField(
                          text: "Title",
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        CustomDateSelector(
                          title: "Date",
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        CustomTimeSelector(
                          title: 'Start Time',
                        )
                      ],
                    ),
                  ),
                  actions: [
                    RaisedButton(
                        onPressed: () {},
                        color: Colors.blueAccent,
                        child: Text(
                          'Schedule',
                          style: TextStyle(color: Colors.white),
                        ))
                  ],
                );
              });
        },
        child: Icon(Icons.add),
      ),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text('Appointments'),
        backgroundColor: Colors.blueAccent,
        actions: [
          // Padding(
          //   padding: EdgeInsets.all(10),
          //   child: GestureDetector(
          //     onTap: (){
          //       if(calendarView==CalendarView.schedule){
          //         setState(() {
          //           calendarView=CalendarView.month;
          //         });
          //       }else{
          //         setState(() {
          //           calendarView=CalendarView.schedule;
          //         });
          //       }
          //     },
          //     child: Icon(
          //         Icons.swap_horiz
          //     ),
          //   ),
          // )
        ],
      ),
      body: SfCalendar(
        showDatePickerButton: true,
        view: calendarView,
        dataSource: events,
        scheduleViewMonthHeaderBuilder: scheduleViewBuilder,
        onTap: _onCalendarTapped,
      ),
    );
  }
}

class _DataSource extends CalendarDataSource {
  _DataSource(List<Appointment> source) {
    appointments = source;
  }
}

Widget scheduleViewBuilder(
    BuildContext buildContext, ScheduleViewMonthHeaderDetails details) {
  final String monthName = _getMonthDate(details.date.month);
  return Stack(
    children: [
      Image(
          image: ExactAssetImage('images/' + monthName + '.png'),
          fit: BoxFit.cover,
          width: details.bounds.width,
          height: details.bounds.height),
      Positioned(
        left: 55,
        right: 0,
        top: 20,
        bottom: 0,
        child: Text(
          monthName + ' ' + details.date.year.toString(),
          style: TextStyle(fontSize: 18),
        ),
      )
    ],
  );
}

String _getMonthDate(int month) {
  if (month == 01) {
    return 'January';
  } else if (month == 02) {
    return 'February';
  } else if (month == 03) {
    return 'March';
  } else if (month == 04) {
    return 'April';
  } else if (month == 05) {
    return 'May';
  } else if (month == 06) {
    return 'June';
  } else if (month == 07) {
    return 'July';
  } else if (month == 08) {
    return 'August';
  } else if (month == 09) {
    return 'September';
  } else if (month == 10) {
    return 'October';
  } else if (month == 11) {
    return 'November';
  } else {
    return 'December';
  }
}
