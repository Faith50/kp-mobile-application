import 'package:flutter/material.dart';

void main() => runApp(new AddAllergies());

class AddAllergies extends StatefulWidget {
  @override
  _AddAllergiesState createState() => _AddAllergiesState();
}

class _AddAllergiesState extends State<AddAllergies> {
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      double appwidth =
          MediaQuery.of(context).size.width; // get the screen width
      double appheight =
          MediaQuery.of(context).size.height; //get the screen hight
      double columnSpace = appwidth / 100.0;
      return new Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text(
              'Add Allergies',
              style: TextStyle(color: Color(0xff2372a3), fontSize: 15.0),
            ),
            centerTitle: true,
            backgroundColor: Colors.white,
          ),
          body: ListView(children: <Widget>[
            new Padding(
              padding: const EdgeInsets.only(top: 20),
            ),
            Container(
              height:
                  orientation == Orientation.landscape ? double.infinity : null,
              padding: const EdgeInsets.all(20.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(children: [
                    Expanded(
                      child: TextField(
                        decoration: InputDecoration(labelText: "Allergy"),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: TextField(
                        decoration: InputDecoration(labelText: "Allergen"),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                  ]),
                  Row(children: [
                    Expanded(
                      child: TextField(
                        decoration:
                            InputDecoration(labelText: "Reactions/Symptoms"),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: TextField(
                        decoration: InputDecoration(labelText: "Condition"),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                  ]),
                  new TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Treatment",
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  new TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Observation",
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 30),
                  ),
                  new MaterialButton(
                    height: 50.0,
                    minWidth: 200,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    color: Color(0xff2372a3),
                    child: new Text(
                      "Save",
                      style: TextStyle(fontSize: 18.0, color: Colors.white),
                    ),
                    onPressed: () => {
                      // Navigator.push(
                      //  context,
                      //MaterialPageRoute(
                      //  builder: (context) => PatientVisitDashboard()))
                    },
                    splashColor: Colors.white,
                  ),
                ],
              ),
            )
          ]));
    });
  }
}

class ClientAllergies extends StatelessWidget {
  final String title;
  final String subtitle;
  ClientAllergies({this.title, this.subtitle});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border:
              Border(bottom: BorderSide(width: 3, color: Colors.grey[200]))),
      child: Padding(
        padding: EdgeInsets.only(top: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title),
            SizedBox(
              height: 15,
            ),
            Text(
              subtitle,
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
