import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() => runApp(new Appointment());

DateTime picked;
DateTime selectedDate = new DateTime.now();

class Appointment extends StatefulWidget {
  @override
  _AppointmentState createState() => _AppointmentState();
}

//Appointmnet states and varibles are set here.
class _AppointmentState extends State<Appointment> {
  final GlobalKey<FormState> _appointmentkey = new GlobalKey<FormState>();
  TextEditingController uuidController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  TextEditingController serviceController = TextEditingController();
  TextEditingController serviceProviderController = TextEditingController();
  TextEditingController remarkController = TextEditingController();

// this build up the widget for appointment and appointment schedulling.
  @override
  Widget build(BuildContext context) {
    double appwidth = MediaQuery.of(context).size.width; // get the screen width
    double appheight =
        MediaQuery.of(context).size.height; //get the screen hight
    double columnSpace = appwidth / 100.0;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff011829),
        title: const Text(
          'Appointments',
        ),
      ),
      body: Container(
        width: appwidth,
        height: appheight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Find Appointments",
                  fillColor: Color.alphaBlend(
                    Colors.white,
                    Color(0xff011829),
                  ),
                  filled: true,
                  suffixIcon: Icon(Icons.search),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                ),
              ),
            ),
            RaisedButton(
              onPressed: () => _selectDate(context),
              child: Text("i am here"),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xff011829),
        onPressed: _appointmentScheduler,
        child: Icon(Icons.add),
      ),
    );
  }

  _appointmentScheduler() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Center(
          child: Text(
            "Schedule Appointments",
          ),
        ),
        content: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            children: [
              Form(
                key: _appointmentkey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("$picked"),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      child: TextField(
                        autofocus: true,
                        controller: uuidController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'UUID',
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      child: TextField(
                        controller: dateController,
                        readOnly: true,
                        enableInteractiveSelection: true,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), labelText: 'Date'),
                        onTap: () {
                          _selectDate(context);
                          print("datecontroller" "${dateController.text}");
                        },
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      child: TextField(
                        autofocus: true,
                        controller: timeController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), labelText: 'Time'),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      child: TextField(
                        autofocus: true,
                        controller: serviceController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), labelText: 'Service'),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      child: TextField(
                        autofocus: true,
                        controller: serviceProviderController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'ServiceProvider'),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      child: TextField(
                        autofocus: true,
                        controller: remarkController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), labelText: 'Remarks'),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: RaisedButton(
                  color: Color(0xff011829),
                  child: Text(
                    "SCHEDULE",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _selectDate(BuildContext context) async {
    DateFormat dateFormat;
    picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2020),
      lastDate: DateTime(2050),
      // initialEntryMode: DatePickerEntryMode.calendar,
    );
    if (picked != null)
      // && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        //selectedDate = picked.toLocal();
        print('pickd is ' '$picked');
        print(selectedDate);
        dateController.text = dateFormat.format(selectedDate);
        print(dateController.text);
      });
  }
}
