import 'package:flutter/material.dart';

void main() => runApp(new AddVitalSign());

class AddVitalSign extends StatefulWidget {
  @override
  _AddVitalSignState createState() => _AddVitalSignState();
}

class _AddVitalSignState extends State<AddVitalSign> {
  @override
  Widget build(BuildContext context) {
    return NewWidget();
  }
}

class NewWidget extends StatelessWidget {
  const NewWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      double appwidth =
          MediaQuery.of(context).size.width; // get the screen width
      double appheight =
          MediaQuery.of(context).size.height; //get the screen hight
      double columnSpace = appwidth / 100.0;
      return new Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text(
              'Add Vital Sign Records',
              style: TextStyle(color: Color(0xff2372a3), fontSize: 15.0),
            ),
            centerTitle: true,
            backgroundColor: Colors.white,
          ),
          body: ListView(children: <Widget>[
            new Padding(
              padding: const EdgeInsets.only(top: 20),
            ),
            Container(
              height:
                  orientation == Orientation.landscape ? double.infinity : null,
              padding: const EdgeInsets.all(20.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(children: [
                    Expanded(
                      child: TextField(
                        decoration: InputDecoration(labelText: "Systolic"),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: TextField(
                        decoration: InputDecoration(labelText: "Diastolic"),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                  ]),
                  new TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Respiratory Rate (breath/min)",
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  new TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Pulse Rate(pulse/min)",
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  new TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Body Temperature(oF)",
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  new TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Height(cm)",
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 20),
                  ),
                  new MaterialButton(
                    height: 50.0,
                    minWidth: 200,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    color: Color(0xff2372a3),
                    child: new Text(
                      "Save",
                      style: TextStyle(fontSize: 18.0, color: Colors.white),
                    ),
                    onPressed: () => {
                      // Navigator.push(
                      //  context,
                      //MaterialPageRoute(
                      //  builder: (context) => PatientVisitDashboard()))
                    },
                    splashColor: Colors.white,
                  ),
                ],
              ),
            )
          ]));
    });
  }
}
