import 'package:flutter/material.dart';
import 'package:kp/views/Clinical/add_vitals_signs.dart';

void main() => runApp(new VitalSign());

class VitalSign extends StatefulWidget {
  @override
  _VitalSignState createState() => _VitalSignState();
}

//Vital signs varibles are set here.
class _VitalSignState extends State<VitalSign> {
  final GlobalKey<FormState> _vitalsignkey = new GlobalKey<FormState>();
  TextEditingController uuidController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  TextEditingController respiratoryrateController = TextEditingController();
  TextEditingController pulseController = TextEditingController();
  TextEditingController bloodpressureProviderController =
      TextEditingController();
  TextEditingController temperatureController = TextEditingController();
  TextEditingController weightController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double appwidth = MediaQuery.of(context).size.width; // get the screen width
    double appheight =
        MediaQuery.of(context).size.height; //get the screen hight
    double columnSpace = appwidth / 100.0;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Vital Signs',
          style: TextStyle(color: Color(0xff2372a3)),
        ),
      ),
      body: Container(
        width: appwidth,
        height: appheight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Search for Vitals",
                  fillColor: Color.alphaBlend(
                    Colors.white,
                    Color(0xff011829),
                  ),
                  filled: true,
                  suffixIcon: Icon(Icons.search),
                  //enabledBorder: OutlineInputBorder(
                  // borderRadius: BorderRadius.circular(15.0),
                  //  ),
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xff2372a3),
        onPressed: () => {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddVitalSign()))
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
