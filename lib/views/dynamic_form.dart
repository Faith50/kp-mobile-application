import 'package:flutter/material.dart';
import 'package:kp/widgets/dynamic_date_time_picker.dart';
import 'package:kp/widgets/dynamic_form.dart';

class TempView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Center(
            child: Padding(
              padding: EdgeInsets.all(10),
              child: DynamicForm(
                config: {
                  "components": [
                    {
                      "components": [
                        {
                          "components": [
                            {
                              "legend": "ART Commencement",
                              "key": "fieldset",
                              "type": "fieldset",
                              "label": "",
                              "input": false,
                              "tableView": false,
                              "components": [
                                {
                                  "label": "Columns",
                                  "columns": [
                                    {
                                      "components": [
                                        {
                                          "label":
                                              "Clinical stage at start of ART",
                                          "widget": "choicesjs",
                                          "tableView": true,
                                          "data": {
                                            "values": [
                                              {
                                                "label": "Asymtomatic Symptoms",
                                                "value": "1"
                                              },
                                              {
                                                "label": "Mild Symptoms",
                                                "value": "2"
                                              },
                                              {
                                                "label": "Advanced Symptoms",
                                                "value": "3"
                                              },
                                              {
                                                "label": "Severe Symptoms",
                                                "value": "4"
                                              }
                                            ]
                                          },
                                          "dataType": "number",
                                          "selectThreshold": 0.3,
                                          "key": "clinicalStageAtStartOfArt",
                                          "type": "select",
                                          "indexeddb": {"filter": {}},
                                          "input": true,
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "width": 4,
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "size": "md"
                                    },
                                    {
                                      "components": [
                                        {
                                          "label": "CD4 at start of ART",
                                          "mask": false,
                                          "spellcheck": true,
                                          "tableView": false,
                                          "delimiter": false,
                                          "requireDecimal": false,
                                          "inputFormat": "plain",
                                          "key": "cd4AtStartOfArt",
                                          "type": "number",
                                          "input": true,
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "size": "md",
                                      "width": 4
                                    },
                                    {
                                      "components": [
                                        {
                                          "label":
                                              "Date Initial ADR Counselling Completed",
                                          "format": "dd-MMM-yyyy",
                                          "tableView": false,
                                          "enableMinDateInput": false,
                                          "datePicker": {
                                            "disableWeekends": false,
                                            "disableWeekdays": false
                                          },
                                          "enableMaxDateInput": false,
                                          "key": "dateTime",
                                          "type": "datetime",
                                          "input": true,
                                          "widget": {
                                            "type": "calendar",
                                            "displayInTimezone": "viewer",
                                            "locale": "en",
                                            "useLocaleSettings": false,
                                            "allowInput": true,
                                            "mode": "single",
                                            "enableTime": true,
                                            "noCalendar": false,
                                            "format": "dd-MMM-yyyy",
                                            "hourIncrement": 1,
                                            "minuteIncrement": 1,
                                            "time_24hr": false,
                                            "minDate": null,
                                            "disableWeekends": false,
                                            "disableWeekdays": false,
                                            "maxDate": null
                                          },
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "size": "md",
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "width": 4
                                    }
                                  ],
                                  "key": "columns",
                                  "type": "columns",
                                  "input": false,
                                  "tableView": false
                                },
                                {
                                  "label": "Columns",
                                  "columns": [
                                    {
                                      "components": [
                                        {
                                          "label": "Date ART Started",
                                          "format": "dd-MMM-yyyy",
                                          "tableView": false,
                                          "enableMinDateInput": false,
                                          "datePicker": {
                                            "disableWeekends": false,
                                            "disableWeekdays": false
                                          },
                                          "enableMaxDateInput": false,
                                          "key": "dateArtStarted",
                                          "type": "datetime",
                                          "input": true,
                                          "widget": {
                                            "type": "calendar",
                                            "displayInTimezone": "viewer",
                                            "locale": "en",
                                            "useLocaleSettings": false,
                                            "allowInput": true,
                                            "mode": "single",
                                            "enableTime": true,
                                            "noCalendar": false,
                                            "format": "dd-MMM-yyyy",
                                            "hourIncrement": 1,
                                            "minuteIncrement": 1,
                                            "time_24hr": false,
                                            "disableWeekends": false,
                                            "disableWeekdays": false
                                          },
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "width": 4,
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "size": "md"
                                    },
                                    {
                                      "components": [
                                        {
                                          "label": "First ART Regimen",
                                          "widget": "choicesjs",
                                          "tableView": true,
                                          "data": {
                                            "values": [
                                              {
                                                "label": "1a = TDF-3TC-EFV",
                                                "value": "TDF-3TC-EFV"
                                              },
                                              {
                                                "label": "1b = AZT-3TC-EFV",
                                                "value": "AZT-3TC-EFV"
                                              }
                                            ]
                                          },
                                          "selectThreshold": 0.3,
                                          "key": "firstArtRegimen",
                                          "type": "select",
                                          "indexeddb": {"filter": {}},
                                          "input": true,
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "size": "md",
                                      "width": 4
                                    },
                                    {
                                      "components": [
                                        {
                                          "label": "Weight (kg)",
                                          "mask": false,
                                          "spellcheck": true,
                                          "tableView": false,
                                          "delimiter": false,
                                          "requireDecimal": false,
                                          "inputFormat": "plain",
                                          "key": "cd4AtStartOfArt2",
                                          "type": "number",
                                          "input": true,
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "size": "md",
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "width": 4
                                    }
                                  ],
                                  "key": "columns4",
                                  "type": "columns",
                                  "input": false,
                                  "tableView": false
                                },
                                {
                                  "label": "Columns",
                                  "columns": [
                                    {
                                      "components": [
                                        {
                                          "label": "Height/Length(m/cm)",
                                          "mask": false,
                                          "spellcheck": true,
                                          "tableView": false,
                                          "delimiter": false,
                                          "requireDecimal": false,
                                          "inputFormat": "plain",
                                          "key": "heightLengthMCm",
                                          "type": "number",
                                          "input": true,
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "width": 4,
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "size": "md"
                                    },
                                    {
                                      "components": [
                                        {
                                          "label": "BMI/MUAC",
                                          "inputMask": "999/999",
                                          "tableView": true,
                                          "key": "textField",
                                          "type": "textfield",
                                          "input": true,
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "size": "md",
                                      "width": 4
                                    },
                                    {
                                      "components": [
                                        {
                                          "label": "Functional Status",
                                          "widget": "choicesjs",
                                          "tableView": true,
                                          "data": {
                                            "values": [
                                              {
                                                "label": "Working",
                                                "value": "working"
                                              },
                                              {
                                                "label": "Ambulance",
                                                "value": "ambulance"
                                              },
                                              {
                                                "label": "Bed Ridden",
                                                "value": "bedRidden"
                                              }
                                            ]
                                          },
                                          "dataType": "number",
                                          "selectThreshold": 0.3,
                                          "key": "clinicalStageAtStartOfArt2",
                                          "type": "select",
                                          "indexeddb": {"filter": {}},
                                          "input": true,
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "size": "md",
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "width": 4
                                    }
                                  ],
                                  "key": "columns3",
                                  "type": "columns",
                                  "input": false,
                                  "tableView": false
                                },
                                {
                                  "label": "Columns",
                                  "columns": [
                                    {
                                      "components": [
                                        {
                                          "label": "Pregnant/Breastfeeding",
                                          "widget": "choicesjs",
                                          "tableView": true,
                                          "data": {
                                            "values": [
                                              {
                                                "label": "Pregnant",
                                                "value": "pregnant"
                                              },
                                              {
                                                "label": "Breast Feeding",
                                                "value": "breastFeeding"
                                              }
                                            ]
                                          },
                                          "selectThreshold": 0.3,
                                          "key": "pregnantBreastfeeding",
                                          "type": "select",
                                          "indexeddb": {"filter": {}},
                                          "input": true,
                                          "hideOnChildrenHidden": false
                                        }
                                      ],
                                      "width": 4,
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "size": "md"
                                    },
                                    {
                                      "components": [],
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "size": "md",
                                      "width": 4
                                    },
                                    {
                                      "components": [],
                                      "size": "md",
                                      "offset": 0,
                                      "push": 0,
                                      "pull": 0,
                                      "width": 4
                                    }
                                  ],
                                  "key": "columns2",
                                  "type": "columns",
                                  "input": false,
                                  "tableView": false
                                }
                              ]
                            },
                            {
                              "type": "button",
                              "label": "Submit",
                              "key": "submit",
                              "disableOnInvalid": true,
                              "input": true,
                              "tableView": false
                            }
                          ],
                          "input": true,
                          "key": "",
                          "tableView": false,
                          "label": ""
                        }
                      ],
                      "input": true,
                      "key": "",
                      "tableView": false,
                      "label": ""
                    }
                  ]
                },
              ),
              // child: DynamicDateTimePicker(config: {
              //   "label": "Date Initial ADR Counselling Completed",
              //   "format": "dd-MMM-yyyy",
              //   "tableView": false,
              //   "enableMinDateInput": false,
              //   "datePicker": {
              //     "disableWeekends": false,
              //     "disableWeekdays": false
              //   },
              //   "enableMaxDateInput": false,
              //   "key": "dateTime",
              //   "type": "datetime",
              //   "input": true,
              //   "widget": {
              //     "type": "calendar",
              //     "displayInTimezone": "viewer",
              //     "locale": "en",
              //     "useLocaleSettings": false,
              //     "allowInput": true,
              //     "mode": "single",
              //     "enableTime": true,
              //     "noCalendar": false,
              //     "format": "dd-MMM-yyyy",
              //     "hourIncrement": 1,
              //     "minuteIncrement": 1,
              //     "time_24hr": false,
              //     "minDate": null,
              //     "disableWeekends": false,
              //     "disableWeekdays": false,
              //     "maxDate": null
              //   },
              //   "hideOnChildrenHidden": false
              // }),
            ),
          ),
        ),
      ),
    );
  }
}
