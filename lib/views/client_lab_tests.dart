import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:kp/api/request_middleware.dart';
import 'package:kp/globals.dart';
import 'package:kp/models/lab.dart';
import 'package:kp/models/lab_patients.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:kp/providers/meta_provider.dart';
import 'package:kp/util.dart';
import 'package:provider/provider.dart';
import 'package:kp/views/patients.dart' as patients;
import 'package:http/http.dart' as http;

class ClientLabTests extends StatefulWidget {
  final String hospitalNumber;
  final bool addAppbar;
  ClientLabTests({this.hospitalNumber, this.addAppbar = true}) {
    assert(hospitalNumber != null);
  }
  @override
  State createState() => ClientLabTestsState();
}

class ClientLabTestsState extends State<ClientLabTests> {
  @override
  Widget build(BuildContext context) {
    Widget body = GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: LayoutBuilder(builder: (context, constraint) {
        return Container(
          color: Colors.white,
          height: constraint.maxHeight,
          width: constraint.maxWidth,
          child: Column(
            children: [
              Expanded(
                  child: OnlinePatients(
                hospitalNumber: widget.hospitalNumber,
              ))
            ],
          ),
        );
      }),
    );
    if (widget.addAppbar == false) {
      return body;
    }
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text('${widget.hospitalNumber}'),
      ),
      body: body,
    );
  }
}

class OnlinePatients extends StatefulWidget {
  final String hospitalNumber;
  OnlinePatients({this.hospitalNumber});
  @override
  State createState() => OnlinePatientsState();
}

class OnlinePatientsState extends State<OnlinePatients> {
  ScrollController scrollController = new ScrollController();
  List<LabPatient> clients;
  bool fetchingClients = true;

  @override
  void initState() {
    super.initState();
    fetchingClients = true;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getClients();
    });

    scrollController.addListener(() {
      Provider.of<AuthProvider>(context, listen: false).resetInactivityTimer();
    });
  }

  Future<void> getClients({String lastGameId}) async {
    print('getting clients');
    setState(() {
      fetchingClients = true;
    });
    try {
      http.Response response = await RequestMiddleWare.makeRequest(
          url: endPointBaseUrl +
              "/listclinicallabtests?client_unique_identifier=${widget.hospitalNumber}",
          method: RequestMethod.GET);
      if (response.statusCode == 200) {
        List clientJsons = JsonDecoder().convert(response.body);
        List<LabPatient> clients = clientJsons.map((json) {
          return LabPatient(
              hospitalNumber: json['client_unique_identifier'],
              orderNumber: json['laboratory_ordered_number'],
              testId: json['laboratory_test_id'],
              status: json['laboratory_test_status'],
              recentOrder: json['ordered_date'] == null
                  ? null
                  : convertStringToDateTime(json['ordered_date']),
              prescribedBy: json['users']['username']);
        }).toList();
        this.clients = clients;
      }
      setState(() {
        fetchingClients = false;
      });
    } catch (e) {
      print('here');
      print(e);
      setState(() {
        fetchingClients = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: Padding(
          padding: EdgeInsets.all(0),
          child: clients == null && fetchingClients == true
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : clients == null && fetchingClients == false
                  ? Center(
                      child: RaisedButton(
                        color: Colors.blueAccent,
                        onPressed: () {
                          getClients();
                        },
                        child: Text('Retry',
                            style: TextStyle(color: Colors.white)),
                      ),
                    )
                  : clients.length == 0
                      ? Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Text(
                              "Nothing to see here yet",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )
                      : RefreshIndicator(
                          child: ListView(
                            controller: scrollController,
                            physics: BouncingScrollPhysics(),
                            children: [
                              TableBuilder(
                                clients: clients,
                              )
                            ],
                          ),
                          onRefresh: () async {
                            await getClients();
                          }),
        )),
      ],
    );
  }
}

class TableBuilder extends StatelessWidget {
  final List<LabPatient> clients;
  TableBuilder({this.clients});
  @override
  Widget build(BuildContext context) {
    List<LabTest> labTests =
        context.select((MetadataProvider metaProvider) => metaProvider.tests);
    List<DataRow> dataRows = clients.map((e) {
      LabTest test;
      if (labTests != null) {
        labTests.forEach((element) {
          if (element.labTestId == e.testId) {
            test = element;
          }
        });
      }
      return DataRow(cells: [
        DataCell(Text(
            e.recentOrder == null ? "" : convertDateToString(e.recentOrder))),
        DataCell(Text(test == null ? "" : test.name)),
        DataCell(Text(e.status)),
        DataCell(Text(e.orderNumber.toString())),
      ]);
    }).toList();
    return SingleChildScrollView(
      child: DataTable(columns: [
        DataColumn(label: Text('Date')),
        DataColumn(label: Text('Test')),
        DataColumn(label: Text('Status')),
        DataColumn(label: Text('Order Number')),
      ], rows: dataRows),
      scrollDirection: Axis.horizontal,
    );
  }
}
