import 'package:flutter/material.dart';
import 'package:flutter_duration_picker/flutter_duration_picker.dart';
import 'package:kp/custom_plugins/veri_finger.dart';
import 'package:kp/db/settings.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:kp/util.dart';
import 'package:provider/provider.dart';

class AnalyticsView extends StatefulWidget {
  @override
  State createState() => AnalyticsState();
}

class AnalyticsState extends State<AnalyticsView> {
  Duration timeout;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraint) {
      return Container(
        color: Colors.grey[100],
        height: constraint.maxHeight,
        width: constraint.maxWidth,
        padding: EdgeInsets.all(10),
        child: Column(
          children: [],
        ),
      );
    });
  }
}
