import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:kp/api/request_middleware.dart';
import 'package:kp/globals.dart';
import 'package:kp/models/client_intake_model.dart';
import 'package:kp/models/lab_patients.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:kp/util.dart';
import 'package:kp/views/lab_order.dart';
import 'package:kp/widgets/custom_text_field.dart';
import 'package:kp/widgets/patient_tile.dart';
import 'package:provider/provider.dart';
import 'package:kp/views/patients.dart' as patients;
import 'package:http/http.dart' as http;

class LabDashboard extends StatefulWidget {
  @override
  State createState() => LabDashboardState();
}

class LabDashboardState extends State<LabDashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
        ),
        onPressed: () async {
          ClientIntake client = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Scaffold(
                        appBar: AppBar(
                          iconTheme: IconThemeData(color: Colors.white),
                          title: Text('Select Client'),
                          backgroundColor: Colors.blueAccent,
                        ),
                        body: Padding(
                          padding: EdgeInsets.all(10),
                          child: patients.OnlinePatients(
                            isSelect: true,
                          ),
                        ),
                      )));
          if (client != null) {
            print(client.hospitalNum);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => LabOrder(
                          client: client,
                        )));
          }
        },
      ),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text('Laboratory Tests'),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: LayoutBuilder(builder: (context, constraint) {
          return Container(
            color: Colors.white,
            height: constraint.maxHeight,
            width: constraint.maxWidth,
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                Expanded(child: OnlinePatients())
              ],
            ),
          );
        }),
      ),
    );
  }
}

class OnlinePatients extends StatefulWidget {
  @override
  State createState() => OnlinePatientsState();
}

class OnlinePatientsState extends State<OnlinePatients> {
  ScrollController scrollController = new ScrollController();
  TextEditingController controller = TextEditingController();
  List<LabPatient> clients;
  List<LabPatient> matchedClients;
  bool showSearchResult = false;
  bool fetchingClients = true;

  @override
  void initState() {
    super.initState();
    fetchingClients = true;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getClients();
    });

    scrollController.addListener(() {
      Provider.of<AuthProvider>(context, listen: false).resetInactivityTimer();
    });
  }

  Future<void> getClients({String lastGameId}) async {
    print('getting clients');
    setState(() {
      fetchingClients = true;
    });
    try {
      http.Response response = await RequestMiddleWare.makeRequest(
          url: endPointBaseUrl +
              "/listtestsordered?userId=${Provider.of<AuthProvider>(context, listen: false).currentUser.userId}",
          method: RequestMethod.GET);
      if (response.statusCode == 200) {
        List clientJsons = JsonDecoder().convert(response.body);
        List<LabPatient> clients = clientJsons.map((json) {
          return LabPatient(
              hospitalNumber: json['client_unique_identifier'],
              recentOrder: json['ordered_date'] == null
                  ? null
                  : convertStringToDateTime(json['ordered_date']),
              prescribedBy: json['users']['username']);
        }).toList();
        this.clients = clients;
      }
      setState(() {
        fetchingClients = false;
      });
    } catch (e) {
      print('here');
      print(e);
      setState(() {
        fetchingClients = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(10),
          child: CustomTextField(
            controller: controller,
            onChanged: (query) {
              if (query.trim().length == 0) {
                setState(() {
                  showSearchResult = false;
                });
              } else {
                List<LabPatient> matchedClients = [];
                this.clients.forEach((client) {
                  try {
                    if (client.hospitalNumber
                        .toLowerCase()
                        .contains(query.trim().toLowerCase())) {
                      matchedClients.add(client);
                      return;
                    }
                  } catch (_) {}
                });
                setState(() {
                  this.matchedClients = matchedClients;
                  showSearchResult = true;
                });
              }
            },
          ),
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
            child: Padding(
          padding: EdgeInsets.all(0),
          child: clients == null && fetchingClients == true
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : clients == null && fetchingClients == false
                  ? Center(
                      child: RaisedButton(
                        color: Colors.blueAccent,
                        onPressed: () {
                          getClients();
                        },
                        child: Text('Retry',
                            style: TextStyle(color: Colors.white)),
                      ),
                    )
                  : clients.length == 0
                      ? Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Text(
                              "Nothing to see here yet",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )
                      : showSearchResult == true
                          ? ListView(
                              controller: scrollController,
                              physics: BouncingScrollPhysics(),
                              children: [
                                TableBuilder(
                                  clients: matchedClients,
                                )
                              ],
                            )
                          : RefreshIndicator(
                              child: ListView(
                                controller: scrollController,
                                physics: BouncingScrollPhysics(),
                                children: [
                                  TableBuilder(
                                    clients: clients,
                                  )
                                ],
                              ),
                              onRefresh: () async {
                                await getClients();
                              }),
        )),
      ],
    );
  }
}

class TableBuilder extends StatelessWidget {
  final List<LabPatient> clients;
  TableBuilder({this.clients});
  @override
  Widget build(BuildContext context) {
    List<DataRow> dataRows = clients.map((e) {
      return DataRow(cells: [
        DataCell(ClientOptions(
          isDashboard: true,
          isLabPatient: true,
          labPatient: e,
        )),
        DataCell(Text(e == null ? "" : e.hospitalNumber)),
        DataCell(Text(
            e.recentOrder == null ? "" : convertDateToString(e.recentOrder))),
      ]);
    }).toList();
    return DataTable(
      columns: [
        DataColumn(label: Text('')),
        DataColumn(label: Text('Unique Id')),
        DataColumn(label: Text('Last Order Date')),
      ],
      rows: dataRows,
      columnSpacing: 5,
    );
  }
}
