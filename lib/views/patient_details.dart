import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kp/custom_plugins/veri_finger.dart';
import 'package:kp/models/client_intake_model.dart';
import 'package:kp/util.dart';
import 'package:kp/views/edit_client.dart';
import 'package:kp/widgets/allergies_tab.dart';
import 'package:kp/widgets/custom_tab_bar.dart';
import 'package:kp/widgets/diagnosis_tab.dart';
import 'package:kp/widgets/patient_tile.dart';
import 'package:kp/widgets/tests_tab.dart';
import 'package:kp/widgets/vitlas_tab.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:kp/widgets/appointments_tab.dart';
import 'package:kp/widgets/medications_tab.dart';

class PatientDetails extends StatefulWidget {
  final ClientIntake client;

  PatientDetails({this.client});
  @override
  State createState() => PatientDetailsState();
}

class PatientDetailsState extends State<PatientDetails> {
  ClientIntake client;
  @override
  void initState() {
    client = widget.client;
    super.initState();
  }

  @override
  void dispose() {
    savedDIagnosis = [];
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('', style: TextStyle(color: Colors.blueAccent)),
        backgroundColor: Colors.white,
        actions: [
          ClientOptions(
            client: client,
            isDashboard: true,
          ),
          SizedBox(
            width: 10,
          )
        ],
      ),
      body: ResponsiveBuilder(builder: (context, sizingInfo) {
        return OrientationBuilder(
          builder: (context, orientation) {
            int chunk;
            if (orientation == Orientation.portrait) {
              if (sizingInfo.isMobile) {
                chunk = 1;
              } else {
                chunk = 2;
              }
            } else {
              chunk = 2;
            }

            List<Widget> sections = [
              NameCard(
                client: client,
              ),
              Container(
                height: 450,
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: CustomTabController(
                    tabsLength: 3,
                    tabs: ['Appontments', 'Daignosis', 'Clinics'],
                    tabViews: [
                      AppointmentsTab(
                        client: widget.client,
                      ),
                      DiagnosisTab(
                        client: widget.client,
                        key: GlobalKey(),
                      ),
                      Container()
                    ],
                  ),
                ),
              ),
              Container(
                height: 450,
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: CustomTabController(
                    tabsLength: 3,
                    tabs: ['Vitals', 'Allergies', 'Medications'],
                    tabViews: [
                      VitalsTab(
                        client: client,
                      ),
                      AllergiesTab(
                        client: client,
                      ),
                      MedicationsTab(client: client),
                    ],
                  ),
                ),
              ),
              Container(
                height: 450,
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: CustomTabController(
                    tabsLength: 2,
                    tabs: ['Radiology', 'Test'],
                    tabViews: [
                      Container(),
                      TestsTab(
                        client: client,
                      )
                    ],
                  ),
                ),
              )
            ];
            return Container(
              color: Colors.grey[100],
              child: Padding(
                padding: EdgeInsets.all(5),
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: [...splitToChunks(sections, chunk)],
                ),
              ),
            );
          },
        );
      }),
    );
  }
}

class InfoCard extends StatelessWidget {
  final ClientIntake client;

  InfoCard({this.client});

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      return Container(
        color: Colors.white,
        height: orientation == Orientation.landscape ? double.infinity : null,
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                      child: InfoChip(
                    title: 'Gender',
                    subtitle: client.gender.name,
                  )),
                  SizedBox(
                    width: 30,
                  ),
                  Expanded(
                      child: InfoChip(
                    title: 'Date of Birth',
                    subtitle: convertDateToString(client.dob),
                  )),
                  SizedBox(
                    width: 30,
                  ),
                  Expanded(
                      child: InfoChip(
                    title: 'Phone Number',
                    subtitle: client.phone,
                  )),
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: InfoChip(
                    title: 'Address',
                    subtitle: '17A, Lokogoma',
                  )),
                  SizedBox(
                    width: 30,
                  ),
                  Expanded(
                      child: InfoChip(
                    title: 'City',
                    subtitle: client.state == null ? "" : client.state,
                  )),
                  SizedBox(
                    width: 30,
                  ),
                  Expanded(
                      child: InfoChip(
                    title: 'Registration Date',
                    subtitle: convertDateToString(client.regDate),
                  )),
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: InfoChip(
                    title: 'Marital Status',
                    subtitle: client.maritalStatus.name,
                  )),
                  SizedBox(
                    width: 30,
                  ),
                  Expanded(child: Container()),
                  SizedBox(
                    width: 30,
                  ),
                  Expanded(child: Container()),
                ],
              )
            ],
          ),
        ),
      );
    });
  }
}

class InfoChip extends StatelessWidget {
  final String title;
  final String subtitle;
  InfoChip({this.title, this.subtitle});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border:
              Border(bottom: BorderSide(width: 3, color: Colors.grey[200]))),
      child: Padding(
        padding: EdgeInsets.only(top: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title),
            SizedBox(
              height: 15,
            ),
            Text(
              subtitle,
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}

class NameCard extends StatelessWidget {
  final ClientIntake client;
  NameCard({this.client});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            backgroundColor: Colors.blueAccent,
            child: Text(
              '${client.firstName == "" ? "" : client.firstName[0].toUpperCase()}${client.surname == "" ? "" : client.surname[0].toUpperCase()}',
              style: TextStyle(fontSize: 30),
            ),
            radius: 50,
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            '${client.firstName} ${client.surname}',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            client.phone,
            style: TextStyle(fontSize: 15, color: Colors.grey),
          ),
          SizedBox(
            height: 15,
          ),
          InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EditClient(
                            client: client,
                          )));
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Edit/View'),
                SizedBox(
                  width: 10,
                ),
                Icon(Icons.edit)
              ],
            ),
          ),
          SizedBox(
            height: 25,
          ),
          InkWell(
            onTap: () {
              VeriFingerSDK.verifyClient(client: client, context: context)
                  .then((value) {
                if (value == false) {
                  showBasicMessageDialog('Verification Failed', context);
                } else if (value == true) {
                  showBasicMessageDialog('Verification Successful', context);
                }
              });
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  Icons.fingerprint,
                  size: 50,
                ),
                SizedBox(
                  width: 10,
                ),
                Text('Verify Client')
              ],
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: InkWell(
              onTap: () {},
              child: Container(
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[300], width: 3),
                    borderRadius: BorderRadius.circular(7)),
                child: Center(
                  child: Text(
                    'Send Message',
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
