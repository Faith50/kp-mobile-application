import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kp/api/request_middleware.dart';
import 'package:kp/globals.dart';
import 'package:kp/models/lab.dart';
import 'package:kp/providers/auth_provider.dart';
import 'package:kp/providers/meta_provider.dart';
import 'package:kp/util.dart';
import 'package:kp/widgets/custom_form_dropdown.dart';
import 'package:kp/widgets/labelled_text_field.dart';
import 'package:kp/widgets/section_header.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class LabView extends StatefulWidget {
  @override
  State createState() => LabViewState();
}

class LabViewState extends State<LabView> {
  LabTestType selectedType;
  LabTest selectedTest;
  List<LabTest> testsDropdown;
  TextEditingController hospitalNumber = TextEditingController();
  TextEditingController note = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    List<LabTestType> testTypes = context
        .select((MetadataProvider metaProvider) => metaProvider.testTypes);
    List<LabTest> tests =
        context.select((MetadataProvider metaProvider) => metaProvider.tests);
    return Scaffold(
      body: SizedBox(
        height: MediaQuery.of(context).size.height - kToolbarHeight,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Container(
              height:
                  (MediaQuery.of(context).size.height - kToolbarHeight) * 0.35,
              color: Colors.blueAccent,
              child: SafeArea(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.white,
                              )),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: GestureDetector(
                              onTap: () {},
                              child: Icon(
                                Icons.search,
                                color: Colors.white,
                              )),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              child: SizedBox(
                height:
                    (MediaQuery.of(context).size.height - kToolbarHeight) * 0.8,
                width: MediaQuery.of(context).size.width * 0.85,
                child: Card(
                    child: Padding(
                  padding: EdgeInsets.all(15),
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    children: [
                      Form(
                        key: formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SectionHeader(
                              text: "Laboratory Order Form",
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            CustomFormDropDown<LabTest>(
                              text: 'Test',
                              items: testsDropdown == null
                                  ? null
                                  : testsDropdown.map((test) {
                                      return DropdownMenuItem<LabTest>(
                                        value: test,
                                        child: Text(test.name),
                                      );
                                    }).toList(),
                              useExternalValue: true,
                              initialValue: selectedTest,
                              value: selectedTest,
                              onChanged: (value) {
                                setState(() {
                                  selectedTest = value;
                                });
                              },
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            LabeledTextField(
                              text: 'Hospital Number',
                              controller: hospitalNumber,
                              validator: (val) {
                                if (val.length == 0) {
                                  return "Enter Hospital Number";
                                }

                                return null;
                              },
                            ),
                            LabeledTextField(
                              text: 'Note',
                              controller: note,
                              lines: 5,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: SizedBox(
                                  height: 60,
                                  child: RaisedButton(
                                      onPressed: () {
                                        Provider.of<AuthProvider>(context,
                                                listen: false)
                                            .resetInactivityTimer();
                                        Navigator.pop(context);
                                      },
                                      child: Text(
                                        'Back',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                      color: Colors.blueAccent),
                                )),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                    child: SizedBox(
                                  height: 60,
                                  child: RaisedButton(
                                      onPressed:
                                          selectedType == null ||
                                                  selectedTest == null
                                              ? null
                                              : () async {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          new FocusNode());
                                                  if (formKey.currentState
                                                      .validate()) {
                                                    Provider.of<AuthProvider>(
                                                            context,
                                                            listen: false)
                                                        .resetInactivityTimer();
                                                    bool value =
                                                        await showBasicConfirmationDialog(
                                                            "Do you want to place this test order?",
                                                            context);
                                                    if (value) {
                                                      try {
                                                        showPersistentLoadingIndicator(
                                                            context);
                                                        print(JsonEncoder()
                                                            .convert([
                                                          {
                                                            'laboratory_test_id':
                                                                selectedTest
                                                                    .labTestId,
                                                            'laboratory_test_type_id':
                                                                selectedTest
                                                                    .labTestTypeId,
                                                            'laboratory_test_note':
                                                                note.text
                                                                    .trim(),
                                                            'prescribed_by_id':
                                                                Provider.of<AuthProvider>(
                                                                        context,
                                                                        listen:
                                                                            false)
                                                                    .currentUser
                                                                    .userId,
                                                            'laboratory_test_status':
                                                                'ordered',
                                                            'prescription_date':
                                                                DateTime.now()
                                                                    .toIso8601String(),
                                                            'hospital_number':
                                                                hospitalNumber
                                                                    .text
                                                                    .trim()
                                                          }
                                                        ]));
                                                        http.Response response =
                                                            await RequestMiddleWare
                                                                .makeRequest(
                                                                    url: endPointBaseUrl +
                                                                        '/postclinicallabtests',
                                                                    method: RequestMethod.POST,
                                                                    body: JsonEncoder().convert([
                                                                      {
                                                                        'laboratory_test_id':
                                                                            selectedTest.labTestId,
                                                                        'laboratory_test_type_id':
                                                                            selectedTest.labTestTypeId,
                                                                        'laboratory_test_note': note
                                                                            .text
                                                                            .trim(),
                                                                        'prescribed_by_id': Provider.of<AuthProvider>(context,
                                                                                listen: false)
                                                                            .currentUser
                                                                            .userId,
                                                                        'laboratory_test_status':
                                                                            'ordered',
                                                                        'prescription_date':
                                                                            DateTime.now().toIso8601String(),
                                                                        'hospital_number': hospitalNumber
                                                                            .text
                                                                            .trim()
                                                                      }
                                                                    ]),
                                                                    headers: {
                                                              "Content-Type":
                                                                  "application/json"
                                                            });
                                                        Navigator.pop(context);
                                                        if (response
                                                                .statusCode ==
                                                            200) {
                                                          showBasicMessageDialog(
                                                              "Order placed successfully",
                                                              context);
                                                          setState(() {
                                                            selectedTest = null;
                                                            selectedType = null;
                                                            note.text = "";
                                                          });
                                                        } else {
                                                          showBasicMessageDialog(
                                                              "Error placing test order. Check details entered",
                                                              context);
                                                        }
                                                      } on SocketException catch (err) {
                                                        print(err);
                                                        showBasicMessageDialog(
                                                            'Connection Error',
                                                            context);
                                                      } catch (err) {
                                                        print(err);
                                                        showBasicMessageDialog(
                                                            'Something went wrong',
                                                            context);
                                                      }
                                                    }
                                                  }
                                                },
                                      child: Text(
                                        'Next',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                      color: Colors.blueAccent),
                                ))
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )),
              ),
              top: (MediaQuery.of(context).size.height - kToolbarHeight) * 0.15,
              left: MediaQuery.of(context).size.width * 0.15 / 2,
            )
          ],
        ),
      ),
    );
  }
}
