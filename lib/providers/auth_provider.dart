import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:kp/api/auth_api.dart';
import 'package:kp/db/auth.dart';
import 'package:kp/db/settings.dart';
import 'package:kp/globals.dart';
import 'package:kp/models/user.dart';
import 'package:kp/util.dart';
import 'package:local_auth/local_auth.dart';

class AuthProvider extends ChangeNotifier {
  User currentUser;
  User savedUser;
  bool canCheckBiometrics;
  AuthState authState;
  bool isFetchingCredentials = true;
  Timer inactivityTimer;
  String authToken;
  Timer midTimer;
  BuildContext context;
  AuthProvider() {
    authState = AuthState.LOGGED_OUT;
    AuthDB.getInstance().getSavedUser().then((value) async {
      isFetchingCredentials = false;
      var localAuth = LocalAuthentication();
      authToken = await AuthDB.getInstance().getAuthToken();
      canCheckBiometrics = await localAuth.canCheckBiometrics;
      if (value != null) {
        this.savedUser = value;
        notifyListeners();
      }
    });
  }

  void logout() async {
    deleteAuthToken();
    try {
      if (Navigator.of(context).canPop()) {
        Navigator.of(context).popUntil(ModalRoute.withName('home'));
      }
    } catch (err) {}
    currentUser = null;
    authState = AuthState.LOGGED_OUT;
    notifyListeners();
  }

  void deleteAuthToken() {
    authToken = null;
    AuthDB.getInstance().deleteAuthToken();
    notifyListeners();
  }

  void setContext(BuildContext context) {
    this.context = context;
    globalBuildContext = context;
  }

  void resetInactivityTimer() async {
    Duration durationSettings = await SettingsDB.getInstance().getTimeout();
    if (durationSettings == null) {
      durationSettings = Duration(minutes: 5);
    }
    inactivityTimer?.cancel();
    midTimer?.cancel();
    midTimer =
        Timer(Duration(seconds: durationSettings.inSeconds - 10), () async {
      bool val = await showBasicConfirmationDialog(
          "You have been inactive. Do you want to be logged out?", context);
      if (val == false) {
        inactivityTimer?.cancel();
        midTimer?.cancel();
        resetInactivityTimer();
      }
      if (val == true) {
        if (Navigator.of(context).canPop()) {
          try {
            Navigator.popUntil(context, ModalRoute.withName('home'));
          } catch (err) {
            print(err.toString());
          }
        }
        currentUser = null;
        authState = AuthState.LOGGED_OUT;
        notifyListeners();
        inactivityTimer?.cancel();
        midTimer?.cancel();
      }
    });
    inactivityTimer = Timer(durationSettings, () {
      if (Navigator.of(context).canPop()) {
        try {
          Navigator.popUntil(context, ModalRoute.withName('home'));
        } catch (err) {
          print(err.toString());
        }
      }
      currentUser = null;
      authState = AuthState.LOGGED_OUT;
      notifyListeners();
      inactivityTimer?.cancel();
    });
  }

  Future<void> loginWithFingerPrint() async {
    var localAuth = LocalAuthentication();
    bool didAuthenticate = await localAuth.authenticateWithBiometrics(
        localizedReason: 'Please authenticate to login');
    if (didAuthenticate) {
      currentUser = savedUser;
      authState = AuthState.LOGGED_IN;
      notifyListeners();
    }
  }

  bool verifyPassword(String password) {
    if (currentUser != null) {
      if (currentUser.password == password) {
        return true;
      }
    }
    return false;
  }

  Future<void> login(String username, String password) async {
    await AuthApi.login(username, password).then((loginResponse) async {
      if (loginResponse != null) {
        loginResponse.user.password = password;
        AuthDB.getInstance().saveUser(user: loginResponse.user);
        currentUser = loginResponse.user;
        savedUser = loginResponse.user;
        authState = AuthState.LOGGED_IN;
        authToken = loginResponse.authToken;
        await AuthDB.getInstance()
            .saveAuthToken(token: loginResponse.authToken);
        notifyListeners();
      }
    }).catchError((err) {
      print(err);
      // no internet connection, use save credential to log user in
      if (err.toString() == 'Connection Error') {
        print('Using offline credential');
        if (savedUser.userId == null) {
          throw ('No internet connection');
        }
        if (username.toLowerCase() == savedUser.username.toLowerCase() &&
            password == savedUser.password) {
          authState = AuthState.LOGGED_IN;
          currentUser = savedUser;
          notifyListeners();
          return;
        }
        throw ('Username or password incorrect');
      } else if (err.toString() == 'This user is unauthorized') {
        //delete offline credentials if it exists because this user is no longer authorized by the admin
        if (savedUser.username.toLowerCase() == username.toLowerCase()) {
          AuthDB.getInstance().deleteSavedUser();
        }
        throw (err);
      } else {
        throw (err);
      }
    });
  }
}

enum AuthState { LOGGED_IN, LOGGED_OUT }
